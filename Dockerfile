# Build mttl and launch expressjs
FROM node:14
WORKDIR /app/
ADD . /app/

RUN apt-get update
RUN apt-get install -y python3 python3-pip cron
RUN python3 -m pip install requests
RUN python3 -m pip install psycopg2
RUN python3 -m pip install validators
RUN echo "192.168.87.55 postgres" >> /etc/hosts
# RUN apt-get install -y cron
RUN echo "9 3 * * * /app/mttl/scripts/oauth/timeoutUsersAndProfiles.py" >> /etc/cron.d/removeOldCookies
RUN echo "0 * * * * rm /app/log.txt" >> /etc/cron.d/hourlyLogRefresh
# RUN echo "39 16 * * 6 /app/mttl/scripts/tests/findCCDchanges.py" >> /etc/cron.d/checkCCDchanges

RUN npm i -g npm@7
RUN npm install
EXPOSE 5000
ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
