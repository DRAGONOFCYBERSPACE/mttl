<!-- 
The first four sections: "Problem to solve", "Intended users", "User experience goal", and "Proposal", are strongly recommended, while the rest of the sections can be filled out during the problem validation or breakdown phase. However, keep in mind that providing complete and relevant information early helps our product team validate the problem and start working on a solution. 
-->

### What problem(s) will it solve?

<!-- Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->

### Who is the intended user(s)?

<!-- 
Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later. Personas are described [here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)


* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
* [Parker (Product Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
* [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [Presley (Product Designer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#presley-product-designer)
* [Sasha (Software Developer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)
* [Rachel (Release Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#rachel-release-manager) 
* [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#alex-security-operations-engineer)
* [Simone (Software Engineer in Test)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#simone-software-engineer-in-test)
* [Allison (Application Ops)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#allison-application-ops) 
* [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#priyanka-platform-engineer)
* [Dana (Data Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#dana-data-analyst)
-->

### Lay out the expected user experience

<!--
What is the single user experience workflow this problem addresses? For example, "The user should be able to use the UI/API/.gitlab-ci.yml with GitLab to <perform a specific task>" [Gitlab UX Docs](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/user-story-mapping/)

Try to use [Gherkin syntax](https://cucumber.io/docs/gherkin/reference/) to describe the feature. 
-->

**Given**  
**When**  
**Then**  


### Proposal

<!--How are we going to solve the problem? Try to include the user journey! [Gitlab Journeys](https://about.gitlab.com/handbook/journeys/#user-journey)-->


### Further details

<!--Include use cases, benefits, goals, or any other details that will help us understand the problem better.-->

### Permissions and Security

<!--
What permissions are required to perform the described actions? Are they consistent with the existing permissions as documented for users, groups, and projects as appropriate? Is the proposed behavior consistent between the UI, API, and other access methods (e.g. email replies)?
-->




### Documentation
<!--
See the Feature Change Documentation Workflow [Gitlab Feature Change Workflow](https://docs.gitlab.com/ee/development/documentation/workflow.html#for-a-product-change)

* Add all known Documentation Requirements in this section. See [Gitlab Dev Docs](https://docs.gitlab.com/ee/development/documentation/feature-change-workflow.html#documentation-requirements)
* If this feature requires changing permissions, update the permissions document. See [Gitlab Permissions](https://docs.gitlab.com/ee/user/permissions.html)
-->


##### Update [User Documentation](https://90cos.gitlab.io/mttl/documentation)
- List any changes necessary to user documentation that this feature affects.

##### Update [Developer Documentation](https://gitlab.com/90cos/mttl/-/wikis/home) 
- List any changes necessary to developer documentation that this feature affects.
  
##### Update [CYT Overview briefing](https://gitlab.com/90cos/cyt/training/cyt-overview-brief)
  - Update overview slides relevant to this feature.

**-Is there any other documentation affected?**


### Availability & Testing

<!--
See the test engineering planning process and reach out to your counterpart Software Engineer in Test for assistance: [Gitlab Test Planning](https://about.gitlab.com/handbook/engineering/quality/test-engineering/#test-planning)

This section needs to be retained and filled in during the workflow planning breakdown phase of this feature proposal, if not earlier.

What risks does this change pose to our availability? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? Will it require cross-browser testing?

Please list the test areas (unit, integration and end-to-end) that needs to be added or updated to ensure that this feature will work as intended. Please use the list below as guidance.
* Unit test changes
* Integration test changes
* End-to-end test change 
-->


### What does success look like, and how can we measure that?
<!--
Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this.
-->
**Success Metrics/Criteria**  
e.g. User submissions will increase

**Acceptance Criteria**
1. User submission data must be trackable


### Links / references



/label ~"office::CYT" ~"CYT::MTTL" ~"backlog::idea"
/milestone %"CYT Backlog"
/confidential
