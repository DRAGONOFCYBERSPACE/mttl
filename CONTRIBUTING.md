# Contributing to the MTTL

Would you like to contribute to the 90th COS MTTL and help us make it even better than it is
today? If so, this guide will contain instructions and guidelines for contributors.

- [Question or Problem?](#question)
- [Issues and Bugs](#bugs)
- [Adding a KSAT](#addkast)
- [Adding a KSAT](#modkast)
- [Contributing Code](#code)
- [Issues to be Worked](#issues)
- [Setting up a Development Environment](#local)
- [Style Rules](#style)
- [Submitting a Merge Request](#MR)

## <a name="question"></a> Got a Question or Problem?

**Do not open issues support questions. We want to use issues for feature requests, TTL requests and bugs.** 

If you have a question, first check out our [wiki](https://gitlab.com/90cos/mttl/-/wikis/home)

If you still have a question, feel free to reach out to a member of CYT

## <a name="bugs"></a> Found a Bug?

If you find a bug in the MTTL, please help us by
[submitting an bug issue](https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Bug) 

## <a name="addksat"></a> Missing a KSAT?

If there is a KSAT that you think ought to be included in the mttl, please
[submit a KSAT issue](https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Add+KSAT)

## <a name="modksat"></a> Problem with a KSAT?

If there is a problem with an existing KSAT, or if you would like to add an additional resource for a KSAT please
[submit a KSAT modification issue](https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Modify+KSAT)

## <a name="code"></a> Want to contribute directly?

If you would like to help us by adding/modifying the MTTL yourself, please familiarize yourself with the [developer wiki](https://gitlab.com/90cos/mttl/-/wikis/How-to%27s-(Developer))

## <a name="issues"></a> Issues to be worked

If you would like to contribute to this project and are looking for something to do, the [issues board](https://gitlab.com/90cos/mttl/-/boards) is a good place to look.

## <a name="local"></a> Setting up your development environment

If you would like to develop for this project, it will be helpful to set up your development environment to run the code locally. Instructions on how to setup Docker for VSCode are available [here](https://gitlab.com/90cos/mttl/-/wikis/How-to%27s-(Developer)#how-to-setup-your-dev-environment), and instructions on how to run the MTTL locally are available [here](https://gitlab.com/90cos/mttl/-/wikis/How-to%27s-(Developer)/3.-Run-the-Frontend-in-the-Dev-Environment)

## <a name="local"></a> Style Rules

- All contributions must pass all pipelines before they can be merged
- All Python code should meet [PEP 8 guidelines](https://www.python.org/dev/peps/pep-0008/)

## <a name="MR"></a> Merge Requests

When submitting a merge request, keep in mind the following practices:
- When working locally in your branch, add multiple commits and only create the merge request when you’re done. This way, GitLab runs only one pipeline for all the commits pushed at once. This will save pipeline minutes.
- Delete branches on merge or after merging them to keep the MTTL repository clean.
- Do one thing at a time. There's no need to merge in several features at once. By doing so, you’ll have faster reviews and your changes will be less likely to break things.
- Do not use capital letters or special chars in branch names.
- Contact a member of the approval group for code review AFTER the pipeline passes.