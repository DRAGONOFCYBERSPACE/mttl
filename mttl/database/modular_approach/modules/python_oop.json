{
  "Python Object Oriented Programming": {
    "uniq_id": "BD_PY_OOP_USRCLS",
    "uniq_name": "oop",
    "description": "Understand how to import modules and module components in Python.;\nDescribe how to utilize PIP to install a Python package.;\nComprehend Python objects;\nExplain the Python keyword super;\nExplain Python object initialization;\nExplain Python object attributes;\nDescribe polymorphism in Python;\nDescribe inheritance in Python;\nDescribe getter and setter functions in Python;\nUnderstand how to implement input validation in Python.;\nUnderstand how to implement exception handling in Python.;\nDescribe the terms and fundamentals associated with object-orientated programming using Python.;\nDiscuss Common Principles of object-oriented programming.;\nDiscuss Code Styling Considerations of object-orientated programming.",
    "objective": "Create, reuse and import modules in Python.;\nUtilize modules in the Python standard library.;\nInstall and utilize a Python package via PIP.;\nWrite a class in Python;\nInstantiate a Python object;\nWrite a class constructor;\nWrite object-oriented programs;\nImplement object inheritance.;\nExpand class functionality using getter and setter functions.;\nUse class methods to modify instantiated class objects.;\nWrite a program to demonstrate input validation in Python.;\nWrite a program to demonstrate exception handling in Python.",
    "content": [
      {
        "description": "Modules in Python",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/modules.html"
      },
      {
        "description": "Packages in Python",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/packages.html"
      },
      {
        "description": "User Classes Pt. 1",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/user_classes.html"
      },
      {
        "description": "User Classes Pt. 2",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/user_classes_pt2.html"
      },
      {
        "description": "Exception handling in Python",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/exceptions.html"
      },
      {
        "description": "Object Oriented Principles in Python",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/oop_principles.html"
      },
      {
        "description": "Object Oriented Terminology Review",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/oop_terminology.html"
      }
    ],
    "performance": [
      {
        "description": "Install and utilize a Python package via PIP.",
        "perf": "https://packaging.python.org/tutorials/installing-packages/"
      },
      {
        "description": "Write a class in Python",
        "perf": "https://www.geeksforgeeks.org/python-classes-and-objects/"
      },
      {
        "description": "Expand class functionality using getter and setter functions.",
        "perf": "https://www.geeksforgeeks.org/getter-and-setter-in-python/"
      },
      {
        "description": "Write a program to demonstrate exception handling in Python.",
        "perf": "https://www.geeksforgeeks.org/python-exception-handling/"
      }
    ],
    "required_tasks": [],
    "required_abilities": [
      {
        "ksat_id": "A0561",
        "completes": true,
        "description": "Demonstrate the ability to create, reuse, and import modules"
      }
    ],
    "required_skills": [
      {
        "ksat_id": "S0026",
        "completes": true,
        "description": "Utilize standard library modules."
      },
      {
        "ksat_id": "S0027",
        "completes": false,
        "description": "Install a Python package using PIP."
      },
      {
        "ksat_id": "S0382",
        "completes": true,
        "description": "Create a class constructor or destructor"
      },
      {
        "ksat_id": "S0379",
        "completes": true,
        "description": "Demonstrate ability to write object-oriented programs"
      },
      {
        "ksat_id": "S0079",
        "completes": true,
        "description": "Validate expected input."
      },
      {
        "ksat_id": "S0080",
        "completes": true,
        "description": "Demonstrate the skill to implement exception handling."
      }
    ],
    "required_knowledge": [
      {
        "ksat_id": "K0013",
        "completes": true,
        "description": "Understand how to import modules and module components."
      },
      {
        "ksat_id": "K0024",
        "completes": true,
        "description": "Understand how to use the Python Standard Library"
      },
      {
        "ksat_id": "K0002",
        "completes": false,
        "description": "Describe Modular Design"
      },
      {
        "ksat_id": "K0733",
        "completes": false,
        "description": "Describe how to install a package with PIP."
      },
      {
        "ksat_id": "K0032",
        "completes": true,
        "description": "Understand how to create a Python class and describe the necessary components"
      },
      {
        "ksat_id": "K0033",
        "completes": false,
        "description": "Describe how to create and implement an object"
      },
      {
        "ksat_id": "K0034",
        "completes": false,
        "description": "Describe differences between an object and a class"
      },
      {
        "ksat_id": "K0037",
        "completes": false,
        "description": "Understand the purpose and use of the keyword \"Super\""
      },
      {
        "ksat_id": "K0038",
        "completes": true,
        "description": "Recognize the signature and purpose of the initialization function of a constructor."
      },
      {
        "ksat_id": "K0040",
        "completes": false,
        "description": "Describe the purpose of a class attribute."
      },
      {
        "ksat_id": "K0687",
        "completes": false,
        "description": "(U) Explain procedural and object-oriented programming paradigms."
      },
      {
        "ksat_id": "K0031",
        "completes": false,
        "description": "Describe the Object-oriented principles necessary to implement polymorphism."
      },
      {
        "ksat_id": "K0036",
        "completes": true,
        "description": "Inheritance"
      },
      {
        "ksat_id": "K0039",
        "completes": false,
        "description": "Describe the purpose of getter and setter functions"
      },
      {
        "ksat_id": "K0710",
        "completes": true,
        "description": "Understand how to implement exception handling"
      },
      {
        "ksat_id": "K0195",
        "completes": false,
        "description": "Understand the difference between input validation vs input sanitization."
      },
      {
        "ksat_id": "K0030",
        "completes": false,
        "description": "Describe the terms and fundamentals associated with object-oriented programming using Python."
      },
      {
        "ksat_id": "K0035",
        "completes": true,
        "description": "Identify advantages of object-oriented programming"
      }
    ],
    "resources": [
      {
        "title": "Object Oriented Programming in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://realpython.com/python3-object-oriented-programming/"
      },
      {
        "title": "Exceptions in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://docs.python.org/3/library/exceptions.html"
      },
      {
        "title": "Modules in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://docs.python.org/3/tutorial/modules.html"
      },
      {
        "title": "Handling Exception in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://wiki.python.org/moin/HandlingExceptions"
      },
      {
        "title": "Classes in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.w3schools.com/python/python_classes.asp"
      },
      {
        "title": "Using PIP in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://pip.pypa.io/en/stable/quickstart/"
      },
      {
        "title": "Classes in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://docs.python.org/3/tutorial/classes.html"
      },
      {
        "title": "Packages in Python",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://docs.python.org/3/tutorial/modules.html#packages"
      }
    ],
    "comments": "generated by rellink to module script",
    "assessment": [
      {
        "description": "Create, reuse and import modules in Python.",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5a.html"
      },
      {
        "description": "Write object-oriented programs in Python, using classes, class constructors, objects, getters, and setters",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5b.html"
      },
      {
        "description": "Write a program to demonstrate input validation and exception handling in Python.",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5c.html"
      },
      {
        "description": "Write a program to demonstrate exception handling in Python.",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5d.html"
      }
    ]
  }
}
