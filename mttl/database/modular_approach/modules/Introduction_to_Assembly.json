{
  "Introduction to Assembly": {
    "uniq_id": "IDF_ASM_CMP_Basic",
    "uniq_name": "Intro_to_ASM",
    "description": "Review computer fundamentals necessary to contextualize Assembly.;\nUnderstand underlying structure and methodology for working with Assembly.;\nDifferentiate data types and registers in Assembly.;\nDescribe Advanced Data Type use in Assembly",
    "objective": "Write programs to move, replace, and swap values in registers using Assembly.;\nWrite programs partially copying data - leveraging and adapting across registers of different sizes.;\nIdentify and access different registers appropriately in Assembly.",
    "content": [
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Basic_Operations/negative_bitwise.html"
      },
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Debugging_ASM_pt1.html",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Debugging_ASM_pt1.html"
      },
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/index.html",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/index.html"
      },
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Adv_types.html",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Adv_types.html"
      },
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Data_Types.html",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/Intro_to_ASM/Data_Types.html"
      }
    ],
    "performance": [
      {
        "description": "Write programs to move, replace, and swap values in registers using Assembly.",
        "perf": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_1/Lab1.nasm"
      },
      {
        "description": "Write programs partially copying data - leveraging and adapting across registers of different sizes.",
        "perf": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm"
      },
      {
        "description": "Identify and access different registers appropriately in Assembly.",
        "perf": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm"
      }
    ],
    "required_tasks": [
      {
        "ksat_id": "T0017",
        "completes": false,
        "description": "(U) Analyze, modify, develop, debug and document software and applications using assembly languages."
      }
    ],
    "required_abilities": [],
    "required_skills": [
      {
        "ksat_id": "S0134",
        "completes": true,
        "description": "Utilize registers and sub-registers."
      },
      {
        "ksat_id": "S0143",
        "completes": false,
        "description": "Utilize a debugger to identify errors in a program."
      }
    ],
    "required_knowledge": [
      {
        "ksat_id": "K0201",
        "completes": false,
        "description": "Describe specifics about the x86 architecture."
      },
      {
        "ksat_id": "K0202",
        "completes": false,
        "description": "Describe specifics about the x86-64 architecture"
      },
      {
        "ksat_id": "K0206",
        "completes": false,
        "description": "Understand the purpose of an instruction/opcode."
      },
      {
        "ksat_id": "K0315",
        "completes": false,
        "description": "Knowledge of low-level computer languages (e.g., assembly languages)."
      },
      {
        "ksat_id": "K0207",
        "completes": false,
        "description": "Know what an operand is as a part of an instruction."
      },
      {
        "ksat_id": "K0209",
        "completes": false,
        "description": "Understand the purpose of an assembler."
      },
      {
        "ksat_id": "K0213",
        "completes": true,
        "description": "Identify and describe the 64-bit registers"
      },
      {
        "ksat_id": "K0214",
        "completes": true,
        "description": "Identify and describe the 32-bit registers"
      },
      {
        "ksat_id": "K0215",
        "completes": true,
        "description": "Identify and describe the lower 16-bit registers"
      },
      {
        "ksat_id": "K0216",
        "completes": true,
        "description": "Identify and describe the high 8-bit registers"
      },
      {
        "ksat_id": "K0217",
        "completes": true,
        "description": "Identify and describe the lower 8-bit registers"
      },
      {
        "ksat_id": "K0223",
        "completes": true,
        "description": "With references and required resources, describe the purpose, use, and concepts of the NASM assembler."
      },
      {
        "ksat_id": "K0224",
        "completes": false,
        "description": "Generating opcodes"
      },
      {
        "ksat_id": "K0225",
        "completes": false,
        "description": "How the assembler works"
      },
      {
        "ksat_id": "K0226",
        "completes": false,
        "description": "Differences between assemblers"
      },
      {
        "ksat_id": "K0767",
        "completes": true,
        "description": "Understand mov instructions."
      },
      {
        "ksat_id": "K0770",
        "completes": false,
        "description": "Understand the purpose of the lea instruction."
      },
      {
        "ksat_id": "K0771",
        "completes": true,
        "description": "Understand the purpose of the xchg instruction."
      },
      {
        "ksat_id": "K0819",
        "completes": false,
        "description": "Describe how to use a GDB debugger to identify errors in a program."
      }
    ],
    "resources": [
      {
        "title": "https://www.csee.umbc.edu/courses/undergraduate/313/spring05/burt_katz/lectures/Lect10/structuresInAsm.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.csee.umbc.edu/courses/undergraduate/313/spring05/burt_katz/lectures/Lect10/structuresInAsm.html"
      },
      {
        "title": "https://www.gnu-pascal.de/gpc/Endianness.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.gnu-pascal.de/gpc/Endianness.html"
      },
      {
        "title": "https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm"
      },
      {
        "title": "https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture"
      },
      {
        "title": "http://www.c-jump.com/CIS77/ASM/DataTypes/lecture.html",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "http://www.c-jump.com/CIS77/ASM/DataTypes/lecture.html"
      },
      {
        "title": "https://unix.stackexchange.com/questions/297982/how-to-step-into-step-over-and-step-out-with-gdb",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://unix.stackexchange.com/questions/297982/how-to-step-into-step-over-and-step-out-with-gdb"
      },
      {
        "title": "https://www.tutorialspoint.com/assembly_programming/assembly_variables.htm",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.tutorialspoint.com/assembly_programming/assembly_variables.htm"
      },
      {
        "title": "https://courses.cs.washington.edu/courses/cse351/13su/lectures/12-structs.pdf",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://courses.cs.washington.edu/courses/cse351/13su/lectures/12-structs.pdf"
      },
      {
        "title": "https://www.geeksforgeeks.org/assembly-language-program-find-largest-number-array/",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.geeksforgeeks.org/assembly-language-program-find-largest-number-array/"
      },
      {
        "title": "https://stackoverflow.com/questions/43562980/swapping-two-int-pointers-in-assembly-x86",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://stackoverflow.com/questions/43562980/swapping-two-int-pointers-in-assembly-x86"
      }
    ],
    "comments": "generated by rellink to module script",
    "assessment": [
      {
        "description": "Write programs to move, replace, and swap values in registers using Assembly.",
        "eval": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_1/Lab1.nasm"
      },
      {
        "description": "Write programs partially copying data - leveraging and adapting across registers of different sizes.",
        "eval": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm"
      },
      {
        "description": "Identify and access different registers appropriately in Assembly.",
        "eval": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_2/Lab2.nasm"
      }
    ]
  }
}
