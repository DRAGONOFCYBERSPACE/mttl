#!/usr/bin/python3

import os
import sys
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from mongo_helpers import find_array_not_empty, find_id_and_append_array

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    # iterate all KSATs with parents
    for item in find_array_not_empty(reqs, 'parent'):
        for parent in item['parent']:
            find_id_and_append_array(
                reqs,
                parent,
                'children',
                item['ksat_id'],
                upsert=False
            )


if __name__ == "__main__":
    main()
