def ksat_list_template(work_roles: list):
    return {
        "title": "Schema for a MTTL requirement JSON file.",
        "definitions": {
            "_id": id_template()
        },
        "type": "array",
        "items": ksat_item_template(work_roles),
        "additionalProperties": False
    }


def ksat_item_template(work_roles: list):
    return {
        "title": "Schema for a MTTL requirement JSON file.",
        "definitions": {
            "_id": id_template()
        },
        "type": "object",
        "properties": {
            "_id": {"$ref": "#/definitions/_id"},
            "ksat_id": {"type": "string", "pattern": "^[KSAT][0-9]{4}$"},
            "description": {"type": "string", "minLength": 1},
            "topic": {"type": "string", "minLength": 0},
            "ksat_type":
            {
                "type": "string",
                "enum": ['knowledge', 'skills', 'abilities', 'tasks']
            },
            "requirement_src": {
                "type": "array",
                "items": {"type": "string", "minLength": 1},
                "minItems": 0
            },
            "requirement_owner": {"type": "string", "minLength": 0},
            "comments": {"type": "string", "minLength": 0}
        },
        "required": [
            "ksat_id",
            "description",
            "parent",
            "topic",
            "requirement_src",
            "requirement_owner",
            "comments"
        ]
    }


def id_template():
    return {
        "type": "object",
        "properties": {
            "$oid": {"type": "string"}
        },
        "required": ["$oid"]
    }


def ksat_wr_spec_template(work_roles: list):
    return {
        "type": "array",
        "items": {
            "type": "object",
            "properties": {
                "_id": {"type": "string", "pattern": "^[KSAT][0-9]{4}$"},
                # TODO: Need to figure out how to include these without
                # Roadmap items. Issue #226 on MTTL
                "work-role":
                {
                    "type": "string",
                    "enum": work_roles + ['SEE', 'VR']
                },
                "proficiency":
                {
                    "type": "string",
                    "pattern": "^([A-Da-d1-4]|([1-4][a-d])|)$"
                },
                "overarching_milestone":
                {
                    "type": "string"
                }
            },
            "required": [
                "work-role",
                "proficiency"
            ]
        }
    }


def rel_link_list_template(work_roles, rel_link_item_template):
    return {
        "title": "Schema for a rel-link JSON list.",
        "type": "array",
        "items": rel_link_item_template(work_roles),
        "additionalProperties": False
    }


def trn_rel_link_item_template(work_roles):
    work_roles += ['VR', 'SEE', 'Instructor', '90COS']
    return {
        "type": "object",
        "properties": {
            # one course to many modules (many rel-link objects)
            "course": {"type": "string"},
            # training repo equivalant to a module
            # one module to many topics (rel-links objects)
            "module": {"type": "string"},
            # top level items in Table of Content
            # one topic to many subjects (many rel-link objects)
            "topic": {"type": "string"},
            # one subject to one rel-link object
            "subject": {"type": "string"},
            "network": {"type": "string"},
            "map_for": {"type": "string", "enum": ["eval", "training"]},
            "KSATs": rel_link_mapping_list_template(),
            "lecture_time": {"type": "integer"},
            "perf_demo_time": {"type": "integer"},
            # part_i generation path ~ part_i_path
            "topic_path": {"type": "string"},
            "references": {
                "type": "array",
                "items": {"type": "string"}
            },
            # TODO: update to real schema
            "lesson_objectives": {
                "type": "array"
            },
            # TODO: update to real schema
            "performance_objectives": {
                "type": "array"
            },
            "work-roles": {
                "type": "array",
                "items": [{"type": "string", "enum": work_roles}]
            }
        },
        "required": [
            "course",
            "module",
            "topic",
            "subject",
            "KSATs",
            "network",
            "map_for",
            "work-roles",
            "topic_path"
        ]
    }


# old to new eval rel link conversion chart
# NETWORK -> network
# uid -> question_id
# version -> language
# proficiency -> question_proficiency
# etc -> estimated_time_to_complete
# workroles -> work-roles
def evl_rel_link_item_template(work_roles):
    work_roles += ['VR', 'SEE', 'Instructor']
    return {
        "type": "object",
        "properties": {
            "test_id": {"type": "string"},
            "test_type":
            {
                "type": "string",
                "enum": ["knowledge", "performance"]
            },
            "question_id": {"type": "string"},
            "question_name": {"type": "string"},
            "topic": {"type": "string"},
            "network": {"type": "string"},
            "language": {"type": "string"},
            "map_for": {"type": "string", "enum": ["eval", "training"]},
            "KSATs": rel_link_mapping_list_template(),
            "question_proficiency":
            {
                "type": "string",
                "pattern": "^([A-Da-d1-4]|([1-4][a-d]))$"
            },
            "complexity": {"type": "integer"},
            "estimated_time_to_complete": {"type": "number"},
            "disabled": {"type": "boolean"},
            "provisioned": {"type": "integer"},
            "attempts": {"type": "integer"},
            "passes": {"type": "integer"},
            "failures": {"type": "integer"},
            "work-roles": {
                "type": "array",
                "items": [{"type": "string", "enum": work_roles}]
            }
        },
        "required": [
            "test_id",
            "test_type",
            "question_id",
            "question_name",
            "topic",
            "network",
            "language",
            "question_proficiency",
            "complexity",
            "estimated_time_to_complete",
            "work-roles",
            "KSATs",
            "map_for"
        ]
    }


def rel_link_mapping_list_template():
    return {
        "type": "array",
        "item": rel_link_mapping_item_template()
    }


def rel_link_mapping_item_template():
    return {
        "type": "object",
        "definitions": {
            "_id": id_template()
        },
        "properties": {
            # "ksat_id": {"$ref": "#/definitions/_id"},
            "item_proficiency":
            {
                "type": "string",
                "pattern": "^([A-Da-d1-4]|([1-4][a-d]))$"
            },
            "url": {"type": "string"}
        },
        "required": [
            "ksat_id",
            "item_proficiency",
            "url"
        ]
    }
