#! /bin/bash 
# apt update
# apt -y install postgresql-client
# The review stage doesn't have apt or psql - we probably won't upload sql data there 
psql $DATABASE_URL < ./mttl/scripts/postgres/mttl_postgres_backup.sql