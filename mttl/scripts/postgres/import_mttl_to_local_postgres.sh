#! /bin/bash 
 [[ ! "$PWD" =~ postgres ]] && cd mttl/scripts/postgres/
bash ./format_mttl_for_postgres.sh
/usr/bin/python3 ./import_json_to_postgres.py

exit 0 
