#!/usr/bin/python3
import psycopg2
import os
import sys
import remove_and_revoke_based_on_cookie
from urllib.parse import urlparse
import time


def get_connection_gl() -> psycopg2.extensions.connection:
    # print("getting connection")
    try:
        glconn = os.getenv('GLCONN')
        result = urlparse(glconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)


def check_for_existing_profile(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM profile WHERE cookie = %s", (cookie_id,))
        profile_exists = curs.fetchone() is not None
        conn.close()
        return profile_exists


def check_for_existing_user(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM gitlab WHERE cookie = %s", (cookie_id,))
        user_exists = curs.fetchone() is not None
        conn.close()
        return user_exists


def get_times():
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT  created_at, cookie FROM gitlab")
        timestamps_and_cookies = curs.fetchall()
        conn.close()
        return timestamps_and_cookies


def get_old_cookie_times():
    old_cookies = []
    epoch_day = 86400  # POSIX day (exact value)
    two_days_ago = time.time() - epoch_day*2
    print("two days ago", two_days_ago)
    the_times = get_times()
    print(two_days_ago)
    for epoch_time in the_times:
        print(epoch_time[0])
        if (float(epoch_time[0]) > two_days_ago):
            print("less than two days old", epoch_time[0])
        else:
            print("more than two days old", epoch_time[0])
            old_cookies.append(epoch_time[1])
    return old_cookies


def remove_stale_cookies():
    stale_cookies = get_old_cookie_times()
    for old_cookie in stale_cookies:
        print("old cookie", old_cookie)
        remove_and_revoke_based_on_cookie.disappear_cookie(old_cookie)


def main():
    remove_stale_cookies()


if __name__ == '__main__':
    main()
