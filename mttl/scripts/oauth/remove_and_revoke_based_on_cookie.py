#!/usr/bin/python3
import psycopg2
import json
import os
import sys
from urllib.parse import unquote
from urllib.parse import urlparse
from requests.auth import HTTPBasicAuth
import requests


def get_connection_gl() -> psycopg2.extensions.connection:
    try:
        glconn = os.getenv('GLCONN')
        result = urlparse(glconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)


def check_for_existing_profile(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM profile WHERE cookie = %s", (cookie_id,))
        profile_exists = curs.fetchone() is not None
        conn.close()
        return profile_exists


def check_for_existing_user(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM gitlab WHERE cookie = %s", (cookie_id,))
        user_exists = curs.fetchone() is not None
        conn.close()
        return user_exists


def get_existing_profile(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM profile WHERE cookie = %s", (cookie_id,))
        profile = curs.fetchone()
        conn.close()
        # print(profile)
        return profile


def get_existing_user(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("SELECT * FROM gitlab WHERE cookie = %s", (cookie_id,))
        user = curs.fetchone()
        conn.close()
        return user


def drop_user_profile(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("DELETE FROM profile WHERE cookie = %s", (cookie_id,))
        conn.commit()
        conn.close()


def drop_user_token(cookie_id):
    conn = None
    conn = get_connection_gl()
    if conn is not None:
        curs = conn.cursor()
        curs.execute("DELETE FROM gitlab WHERE cookie = %s", (cookie_id,))
        conn.commit()
        conn.close()


def remove_token(access_token):
    cid = "f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1"
    client_id = cid
    try:
        part_two = os.getenv('PART_TWO')
    except Exception:
        sys.exit("PART_TWO does not exist in environment. Exiting.")

    payload = {"token": access_token,
               "token_type_hint": "refresh_token"
               }
    auth = HTTPBasicAuth(client_id, part_two)
    res = requests.post("https://gitlab.com/oauth/revoke",
                        data=payload,
                        auth=auth,
                        )
    print(res.text)
    response = res.json()
    print("Response from Python removeToken")
    print(json.dumps(response, indent=4, sort_keys=True))
    return(response)


def revoke_based_on_cookie(cookie_id):
    access_token = None
    if(check_for_existing_user(cookie_id)):
        user = get_existing_user(cookie_id)
        print(user)
        access_token = user[1]
    else:
        print("user not found")
    if access_token is not None:
        remove_token(access_token)
        print("token revoked")


def disappear_cookie(cookie_id):
    revoke_based_on_cookie(cookie_id)
    if (check_for_existing_profile(cookie_id)):
        drop_user_profile(cookie_id)
    else:
        print("profile not found")

    if(check_for_existing_user(cookie_id)):
        drop_user_token(cookie_id)
    else:
        print("user not found")


def main():
    if not sys.argv[1]:
        print("cookieId was not given. This is an error.")
    else:
        cookie_id = unquote(sys.argv[1])
    disappear_cookie(cookie_id)


if __name__ == '__main__':
    main()
