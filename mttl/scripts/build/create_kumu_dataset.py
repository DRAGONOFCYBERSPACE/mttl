#!/usr/bin/python3

import os
import json
import pymongo

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
wrs = db.work_roles


def key_picker(key: str):
    """
    Helper function that will pick the correct string from the key identifier
    """
    return {
        'K': 'Knowledge',
        'S': 'Skills',
        'A': 'Abilities',
        'T': 'Tasks'
    }[key[0]]


def main():
    elements = []
    connections = []
    mttldata = reqs.find({})

    # add label and type fields and from/to
    for ksat in mttldata:
        key = ksat['ksat_id']
        elements.append({
            'Label': key,
            'Type': key_picker(key),
            'Description': ksat['description'],
            'Topic': ksat['topic'],
            'OPR': ksat['requirement_owner'],
            'Source': ksat['requirement_src']

        })
        for parent in ksat['parent']:
            connections.append({
                'From': key,
                'To': reqs.find_one({'_id': parent})['ksat_id']
            })

    kumudata = {
        'elements': elements,
        'connections': connections
    }

    with open('kumu-data.json', 'w') as kumufile:
        json.dump(kumudata, kumufile, sort_keys=False, indent=4)


if __name__ == "__main__":
    main()
