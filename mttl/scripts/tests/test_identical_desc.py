#!/usr/bin/python3

import os
import json
import pymongo

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

filename = os.path.splitext(os.path.basename(__file__))[0]

query_group = {
    '$group': {
        '_id': {
            'description': '$description'
        },
        'duplicates': {'$addToSet': '$ksat_id'},
        'count': {'$sum': 1}
    }
}
query_match = {
    '$match': {
        'count': {'$gt': 1}
    }
}
query_sort = {
    '$sort': {
        'count': -1
    }
}


def query_dup_description(collection) -> list:
    '''
    Function to aggregate all ksats with duplicate descriptions
    '''
    return list(collection.aggregate([query_group, query_match, query_sort]))


def query_dup_topic_description(collection) -> list:
    '''
    Function to aggregate all ksats with duplicate descriptions and topics
    '''
    query_group['$group']['_id'].update({'topic': '$topic'})
    return list(collection.aggregate([query_group, query_match, query_sort]))


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    matched_description = list(query_dup_description(reqs))
    matched_topic_description = list(query_dup_topic_description(reqs))

    if len(matched_description) > 0:
        with open(f'{filename}.json', 'w+') as log_file:
            json.dump({
                'dup_topics_&_description': matched_topic_description,
                'dup_description': matched_description
            }, log_file, sort_keys=False, indent=4)
        print('There are errors present. Read the '
              f'\'{filename}.json\' artifact file')
        if len(matched_topic_description) > 0:
            exit(1)


if __name__ == "__main__":
    main()
