#! /usr/bin/python3

import os
import json
import sys
import get_new_record

# new KSATs
new_tasks_file = "newTasks.json"
new_abilities_file = "newAbilities.json"
new_skills_file = "newSkills.json"
new_knowledge_file = "newKnowledge.json"
# new work roles
new_90cos_work_role_file = "new90COS.json"
new_ac_work_role_file = "newAC.json"
new_ccd_work_role_file = "newCCD.json"
new_cst_work_role_file = "newCST.json"
new_instr_work_role_file = "newInstructor.json"
new_jsre_work_role_file = "newJSRE.json"
new_mccd_work_role_file = "newMCCD.json"
new_po_work_role_file = "newPO.json"
new_sad_work_role_file = "newSAD.json"
new_sccdl_work_role_file = "newSCCD-L.json"
new_sccdw_work_role_file = "newSCCD-W.json"
new_see_work_role_file = "newSEE.json"
new_sm_work_role_file = "newSM.json"
new_spo_work_role_file = "newSPO.json"
new_ssm_work_role_file = "newSSM.json"
new_ssre_work_role_file = "newSSRE.json"
new_sysadmin_work_role_file = "newSysadmin.json"
new_tae_work_role_file = "newTAE.json"


def delete_old_new_files():
    old_news = [
        new_ccd_work_role_file, new_tasks_file,
        new_abilities_file, new_skills_file, new_knowledge_file,
        new_90cos_work_role_file, new_ac_work_role_file,
        new_ccd_work_role_file, new_cst_work_role_file,
        new_instr_work_role_file, new_jsre_work_role_file,
        new_mccd_work_role_file, new_po_work_role_file, new_sad_work_role_file,
        new_sccdl_work_role_file, new_sccdw_work_role_file,
        new_see_work_role_file, new_sm_work_role_file,
        new_spo_work_role_file, new_ssm_work_role_file,
        new_ssre_work_role_file, new_sysadmin_work_role_file,
        new_tae_work_role_file]
    for old in old_news:
        if os.path.exists(old):
            os.remove(old)


# def listKSATsforNewWorkRole(workrole):
#     if workrole == "CCD"

def get_work_role_file(workrole):
    if workrole == "90COS":
        return new_90cos_work_role_file
    elif workrole == "AC":
        return new_ac_work_role_file
    elif workrole == "CCD":
        return new_ccd_work_role_file
    elif workrole == "CST":
        return new_cst_work_role_file
    elif workrole == "Instructor":
        return new_instr_work_role_file
    elif workrole == "JSRE":
        return new_jsre_work_role_file
    elif workrole == "MCCD":
        return new_mccd_work_role_file
    elif workrole == "PO":
        return new_po_work_role_file
    elif workrole == "SAD":
        return new_sad_work_role_file
    elif workrole == "SCCDL":
        return new_sccdl_work_role_file
    elif workrole == "SCCDW":
        return new_sccdw_work_role_file
    elif workrole == "SEE":
        return new_see_work_role_file
    elif workrole == "SM":
        return new_sm_work_role_file
    elif workrole == "SPO":
        return new_spo_work_role_file
    elif workrole == "SPO":
        return new_spo_work_role_file
    elif workrole == "SPO":
        return new_spo_work_role_file
    elif workrole == "SPO":
        return new_spo_work_role_file
    elif workrole == "SPO":
        return new_spo_work_role_file
    elif workrole == "SPO":
        return new_spo_work_role_file
    else:
        print("Unable to identify {} as a valid work role. ", workrole)
        sys.exit()


def find_work_role_ksats(workrole):
    work_role_ksats = []
    work_role_file = get_work_role_file(workrole)
    with open(work_role_file) as ccd_check:
        ccd_data = json.load(ccd_check)
        ttl = (ccd_data['TTL'])
        for ksat in ttl:
            current_ksat = (ksat["ksat_id"])
            work_role_ksats.append(current_ksat)
        return list(set(work_role_ksats))


def load_knowledge():
    ksat_file = new_knowledge_file
    with open(ksat_file) as ksat_check:
        knowledge_data = json.load(ksat_check)
        return knowledge_data


def load_skills():
    ksat_file = new_skills_file
    with open(ksat_file) as ksat_check:
        skills_data = json.load(ksat_check)
        return skills_data


def load_abilities():
    ksat_file = new_abilities_file
    with open(ksat_file) as ksat_check:
        abilities_data = json.load(ksat_check)
        return abilities_data


def load_tasks():
    ksat_file = new_tasks_file
    with open(ksat_file) as ksat_check:
        tasks_data = json.load(ksat_check)
        return tasks_data


def get_work_role_expectations(ksats):
    great_expectations = []
    knowledge_data = load_knowledge()
    skills_data = load_skills()
    abilities_data = load_abilities()
    task_data = load_tasks()
    # Q: why not a PG query ?
    # A: that's fine for new/current - \
    # but there is not infinite database  for all KSAT versions
    # Either way the old data has to be loaded
    for line in knowledge_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    for line in skills_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    for line in abilities_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    for line in task_data:
        for ksat in ksats:
            if ksat == line["ksat_id"]:
                great_expectations.append(line)
    return great_expectations


def get_new_work_role_ksats(workrole):
    if workrole not in ["90COS", "AC", "CCD", "CST", "INSTRUCTOR", "JSRE",
                                 "MCCD", "PO", "SAD", "SCCDL", "SCCDW", "SEE",
                                 "SM", "SPO", "SSM", "SSRE",
                                 "SYSADMIN", "TAE"]:
        print("Error Invalid Work Role specified ", workrole)
        sys.exit()
    else:
        print("Getting KSATs for ", workrole)
        work_role_ksats = find_work_role_ksats(workrole)
        work_role_expectations = get_work_role_expectations(work_role_ksats)
        return work_role_expectations


def main(file_to_get):
    delete_old_new_files()
    file_to_get = file_to_get.upper()
    file_to_get = ''.join(e for e in file_to_get if e.isalnum())
    print("Getting current {} work role file.".format(file_to_get))
    get_new_record.main(file_to_get)
    for skill in ["K", "S", "A", "T"]:
        get_new_record.main(skill)
    return(get_new_work_role_ksats(file_to_get))


if __name__ == "__main__":
    file_to_get = sys.argv[1]
    main(file_to_get)
