#! /usr/bin/python3
import sys
import subprocess

# new KSATs
new_tasks_file = "newTasks.json"
new_abilities_file = "newAbilities.json"
new_skills_file = "newSkills.json"
new_knowledge_file = "newKnowledge.json"
# new work roles
new_90cos_work_role_file = "new90COS.json"
new_ac_work_role_file = "newAC.json"
new_ccd_work_role_file = "newCCD.json"
new_cst_work_role_file = "newCST.json"
new_instr_work_role_file = "newInstructor.json"
new_jsre_work_role_file = "newJSRE.json"
new_mccd_work_role_file = "newMCCD.json"
new_po_work_role_file = "newPO.json"
new_sad_work_role_file = "newSAD.json"
new_sccdl_work_role_file = "newSCCD-L.json"
new_sccdw_work_role_file = "newSCCD-W.json"
new_see_work_role_file = "newSEE.json"
new_sm_work_role_file = "newSM.json"
new_spo_work_role_file = "newSPO.json"
new_ssm_work_role_file = "newSSM.json"
new_ssre_work_role_file = "newSSRE.json"
new_sysadmin_work_role_file = "newSysadmin.json"
new_tae_work_role_file = "newTAE.json"
# current work role files
ninety_cos_file = "90COS.json"
ac_file = "AC.json"
ccd_file = "CCD.json"
cst_file = "CST.json"
instr_file = "Instructor.json"
jsre_file = "JSRE.json"
mccd_file = "MCCD.json"
po_file = "PO.json"
sad_file = "SAD.json"
sccdl_file = "SCCD-L.json"
sccd_file = "SCCD-W.json"
see_file = "SEE.json"
sm_file = "SM.json"
spo_file = "SPO.json"
ssm_file = "SSM.json"
ssre_file = "SSRE.json"
sysadmin_file = "Sysadmin.json"
tae_file = "TAE.json"


new_ksats = []
new_expectations = []


def write_new_file(rev_file, new_work_role_file):
    with open(new_work_role_file, 'w') as write_here:
        p = subprocess.Popen(['git', 'show', rev_file], stdout=write_here)
        p.wait()


def get_new_ccd_work_role_file(rev1):
    new_nile_name = ":mttl/database/work-roles/" + ccd_file
    rev_file = "{}{}".format(str(rev1), new_nile_name)
    write_new_file(rev_file, new_ccd_work_role_file)


def get_new_sccdl_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + sccdl_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_sccdl_work_role_file)


def get_new_sccdw_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + sccd_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_sccdw_work_role_file)


def get_new_mccd_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + mccd_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_mccd_work_role_file)


def get_new_po_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + po_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_po_work_role_file)


def get_new_spo_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + spo_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_spo_work_role_file)


def get_new_sm_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + sm_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_sm_work_role_file)


def get_new_ssm_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + ssm_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_ssm_work_role_file)


def get_new_ac_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + ac_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_ac_work_role_file)


def get_new_90cos_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + ninety_cos_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_90cos_work_role_file)


def get_new_tae_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + tae_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_tae_work_role_file)


def get_new_see_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + see_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_see_work_role_file)


def get_new_jsre_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + jsre_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_jsre_work_role_file)


def get_new_sad_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + sad_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_sad_work_role_file)


def get_new_sysadmin_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + sysadmin_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_sysadmin_work_role_file)


def get_new_instructor_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + instr_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_instr_work_role_file)


def get_new_ssre_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + ssre_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_ssre_work_role_file)


def get_new_cst_work_role_file(rev1):
    new_file_name = ":mttl/database/work-roles/" + cst_file
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_ssre_work_role_file)


def get_new_work_role_file(file_to_get, rev1):
    if file_to_get == "CCD":
        get_new_ccd_work_role_file(rev1)
    elif file_to_get == "SCCDL":
        get_new_sccdl_work_role_file(rev1)
    elif file_to_get == "SCCDW":
        get_new_sccdw_work_role_file(rev1)
    elif file_to_get == "MCCD":
        get_new_mccd_work_role_file(rev1)
    elif file_to_get == "SM":
        get_new_sm_work_role_file(rev1)
    elif file_to_get == "SSM":
        get_new_ssm_work_role_file(rev1)
    elif file_to_get == "AC":
        get_new_ac_work_role_file(rev1)
    elif file_to_get == "CST":
        get_new_cst_work_role_file(rev1)
    elif file_to_get == "TAE":
        get_new_tae_work_role_file(rev1)
    elif file_to_get == "SEE":
        get_new_see_work_role_file(rev1)
    elif file_to_get == "INSTRUCTOR":
        get_new_instructor_work_role_file(rev1)
    elif file_to_get == "SYSADMIN":
        get_new_sysadmin_work_role_file(rev1)
    elif file_to_get == "SAD":
        get_new_sad_work_role_file(rev1)
    elif file_to_get == "JSRE":
        get_new_jsre_work_role_file(rev1)
    elif file_to_get == "90COS":
        get_new_90cos_work_role_file(rev1)
    elif file_to_get == "AC":
        get_new_ac_work_role_file(rev1)
    else:
        print("Invalid File selection: {}.".format(file_to_get))
        print("Please choose from a Work Role file:  90COS,AC,CCD,CST,Instructor,\
JSRE,MCCD,PO,SAD,SCCD-L,SCCD-W,SEE,SM,SPO,SSM,SSRE,Sysadmin,TAE")
        print("or ")
        print("Choose a requirements file: ABILITIES, KNOWLEDGE, SKILLS,\
TASKS")
        sys.exit()


def get_new_task_file(rev1):
    new_file_name = ":mttl/database/requirements/TASKS.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_tasks_file)


def get_new_abilities_file(rev1):
    new_file_name = ":mttl/database/requirements/ABILITIES.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_abilities_file)


def get_new_skills_file(rev1):
    new_file_name = ":mttl/database/requirements/SKILLS.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_skills_file)


def get_new_knowledge_file(rev1):
    new_file_name = ":mttl/database/requirements/KNOWLEDGE.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, new_knowledge_file)


def get_new_ksat_file(file_to_get, rev1):
    if (file_to_get[0]) == 'T':
        get_new_task_file(rev1)
    elif (file_to_get[0]) == 'A':
        get_new_abilities_file(rev1)
    elif (file_to_get[0]) == 'S':
        get_new_skills_file(rev1)
    elif (file_to_get[0]) == 'K':
        get_new_knowledge_file(rev1)
    else:
        print("Error retrieving invalid new KSAT filename for: ", file_to_get)


def get_new_actp_linux_file_rl(rev1):
    new_file_name = ":mttl/database/rel-links/training/\
ACTP_Linux.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newACTP_Linux.rel-links.json")


def get_new_all_personnel_rl(rev1):
    new_file_name = \
        ":mttl/database/rel-links/training/All_Personnel.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newAll_Personnel.rel-links.json")


def get_new_ghidra_rl(rev1):
    new_file_name = ":mttl/database/rel-links/training/Ghidra.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newGhidra.rel-links.json")


def get_new_idf_rl(rev1):
    new_file_name = ":mttl/database/rel-links/training/IDF.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newIDF.rel-links.json")


def get_new_professional_scrum_product_owner_irl(rev1):
    new_file_name = \
        ":mttl/database/rel-links/training/\
Professional_Scrum_Product_Owner_I.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(
        rev_file, "newProfessional_Scrum_Product_Owner_I.rel-links.json")


def get_new_see_trl(rev1):
    new_file_name = ":mttl/database/rel-links/training/SEE.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newSEE.training.rel-links.json")


def get_new_tae_rl(rev1):
    new_file_name = ":mttl/database/rel-links/training/TAE.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newTAE.rel-links.json")


def get_new_training_file(file_to_get, rev1):
    print("getting Training rel link", file_to_get)
    first_two = file_to_get[0:2]
    if first_two == "AC":
        get_new_actp_linux_file_rl(rev1)
    elif first_two == "AL":
        get_new_all_personnel_rl(rev1)
    elif first_two == "GH":
        get_new_ghidra_rl(rev1)
    elif first_two == "ID":
        get_new_idf_rl(rev1)
    elif first_two == "PR":
        get_new_professional_scrum_product_owner_irl(rev1)
    elif first_two == "SE":
        get_new_see_trl(rev1)
    else:
        print("Invalid Training Rel-Link Specified: ", file_to_get)
        sys.exit()

# eval rel-links


def get_new_ccd_rl(rev1):
    new_file_name = ":mttl/database/rel-links/eval/CCD.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newCCD.rel-links.json")


def get_new_po_rl(rev1):
    new_file_name = ":mttl/database/rel-links/eval/PO.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newPO.rel-links.json")


def get_new_sccdl_rl(rev1):
    new_file_name = ":mttl/database/rel-links/eval/SCCD-L.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newSCCD-L.rel-links.json")


def get_new_sccdw_rl(rev1):
    new_file_name = ":mttl/database/rel-links/eval/SCCD-W.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newSCCD-W.rel-links.json")


def get_new_see_rl(rev1):
    new_file_name = ":mttl/database/rel-links/eval/SEE.rel-links.json"
    rev_file = "{}{}".format(str(rev1), new_file_name)
    write_new_file(rev_file, "newSEE.eval.rel-links.json")


def get_new_eval_file(file_to_get, rev1):
    print("getting new Eval rel link file ", file_to_get)
    first_two = file_to_get[0:2]
    if first_two == "CC":
        get_new_ccd_rl(rev1)
    elif first_two == "PO":
        get_new_po_rl(rev1)
    elif first_two == "SC":
        if file_to_get == "SCCD-L":
            get_new_sccdl_rl(rev1)
        elif file_to_get == "SCCD-W":
            get_new_sccdw_rl(rev1)
    elif first_two == "SE":
        get_new_see_rl(rev1)
    else:
        print("Invalid Eval  Rel-Link Specified: ", file_to_get)
        sys.exit()


def get_new_rel_links_file(file_to_get, rev1):
    orig = file_to_get
    file_to_get = file_to_get.split(".")[0].upper()
    if file_to_get in ["ACTP_LINUX", "ALL_PERSONNEL", "GHIDRA",
                       "IDF", "PROFESSIONAL_SCRUM_PRODUCT_OWNER_I",
                       "SEET", "TAE", "ACTP", "ALL", "PRO"]:
        get_new_training_file(file_to_get, rev1)
    elif file_to_get in ["CCD", "PO", "SCCD-L", "SCCD-W", "SEE"]:
        get_new_eval_file(file_to_get, rev1)
    else:
        print("Invalid rel-link file specified: ", orig)


def get_new_file(file_to_get, rev1):
    if file_to_get in ["90COS", "AC", "CCD", "CST", "INSTRUCTOR", "JSRE",
                       "MCCD", "PO", "SAD", "SCCDL", "SCCDW", "SEE",
                       "SM", "SPO", "SSM", "SSRE", "SYSADMIN", "TAE"]:
        get_new_work_role_file(file_to_get, rev1)
    elif file_to_get in ["ACTP_Linux.rel-links", "All_Personnel.rel-links",
                         "Ghidra.rel-links",
                         "IDF.rel-links",
                         "Professional_Scrum_Product_Owner_I.rel-links",
                         "SEE.rel-links", "TAE.rel-links", "CCD.rel-links",
                         "PO.rel-links",
                         "SCCD-L.rel-links", "SCCD-W.rel-links",
                         "SEE.rel-links",
                         "ACTP_LINUX.REL-LINKS",
                         "ALL_PERSONNEL.REL-LINKS",
                         "GHIDRA.REL-LINKS", "IDF.REL-LINKS",
                         "PROFESSIONAL_SCRUM_PRODUCT_OWNER_I.REL-LINKS",
                         "SEE.REL-LINKS", "TAE.REL-LINKS",
                         "ACTP_LINUX.REL", "ALL_PERSONNEL.REL",
                         "GHIDRA.REL", "IDF.REL",
                         "PROFESSIONAL_SCRUM_PRODUCT_OWNER_I.REL",
                         "SEE.REL", "TAE.REL", "SEET.REL",
                         "SCCD-L.REL", "SCCD-W.REL"]:
        get_new_rel_links_file(file_to_get, rev1)
    elif file_to_get in ["K", "S", "A", "T", "ABILITIES",
                         "KNOWLEDGE", "SKILLS", "TASKS"]:
        get_new_ksat_file(file_to_get, rev1)
    else:
        print("Invalid File selection: {}.".format(file_to_get))
        print("Please choose from a Work Role file:  90COS,AC,CCD,CST,\
Instructor,JSRE,MCCD,PO,SAD,SCCD-L,SCCD-W,\
SEE,SM,SPO,SSM,SSRE,Sysadmin,TAE")
        print("or ")
        print("Choose a requirements file:\
ABILITIES, KNOWLEDGE, SKILLS, TASKS")
        print("or")
        print("Choose a training or eval rel-link file i.e.")
        print(" Training: ACTP_LINUX, ALL_PERSONNEL,GHIDRA,\
IDF,PROFESSIONAL_SCRUM_PRODUCT_OWNER_I,\
SEET,TAE, ACTP, ALL, PRO (use 'SEET' for SEE Training)")
        print(" Eval: CCD,PO,SCCD-L,\
SCCD-W,SEE")
        print("     - followed by .rel or \
.rel-links to specify the rel-link file")
        sys.exit()
# timeString = "--before=\"{}\"".format(timeAgo)


def main(file_to_get):
    rev1 = subprocess.check_output(
        ['git', 'rev-list', '-n1', 'master']).decode("utf-8").strip()
    file_to_get = file_to_get.upper()
    if file_to_get.find("REL"):
        get_new_file(file_to_get, rev1)
    else:
        file_to_get = ''.join(e for e in file_to_get if e.isalnum())
        get_new_file(file_to_get, rev1)


if __name__ == "__main__":
    file_to_get = sys.argv[1]
    main(file_to_get)
