#!/usr/bin/python3

import os
import pymongo


HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

log_file = "TestParentsLog.log"

all_id_query = {
    "ksat_id": {
        "$exists": 1
    }
}

projection_query = {
    "ksat_id": 1.0
}

all_ksats_with_parents_query = {
    "parent": {
        "$not": {
            "$size": 0
        }
    }
}

projection_query_parent = {
    "ksat_id": 1.0,
    "parent": 1.0
}


def query_all_ksats_ids(collection) -> list:
    '''
    Function to query and find all ksats
    '''
    all_ksats = []
    for ksat in list(collection.find(all_id_query, projection_query)):
        all_ksats.append(ksat.get("_id"))
    return all_ksats


def query_all_ksats_with_parents_ids(collection) -> list:
    '''
    Function to query and find all ksats with parents
    '''
    all_ksats_w_parents = []
    for ksat in list(collection.find(
            all_ksats_with_parents_query,
            projection_query_parent)):
        all_ksats_w_parents.append(ksat)
    return all_ksats_w_parents


def write_log(ksats_with_bad_parents):
    '''
    Function to write the log of the validate parents
    '''
    f = open(log_file, "w")
    f.write("LIST OF KSATS WITH INVALID PARENTS:\n\n")
    for line in ksats_with_bad_parents:
        f.write(f"KSAT {line[0]} does not have a valid parent. "
                "Offending parent is {line[1]}")
        f.write("\n")
    f.close()


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    # Get a list of all ksats
    all_ksats = list(query_all_ksats_ids(reqs))

    # Get all ksats with parents
    all_ksats_w_parents = list(query_all_ksats_with_parents_ids(reqs))

    # Loop through the parent ksats and check that the parent is a valid ksat
    ksats_with_bad_parents = []
    for ksat_w_parent in all_ksats_w_parents:
        for parent in ksat_w_parent.get("parent"):
            if parent not in all_ksats:
                ksats_with_bad_parents.append(
                    ksat_w_parent.get('ksat_id'),
                    parent)

    # If bad parent exist, exit 1
    # (we are allowing the pipeline to pass for now)
    if len(ksats_with_bad_parents) > 0:
        # Report the bad parents
        write_log(ksats_with_bad_parents)
        exit(1)

    # All parents are good
    if len(ksats_with_bad_parents) == 0:
        f = open(log_file, "w")
        f.write("All parents are valid.")
        f.close()


if __name__ == "__main__":
    main()
