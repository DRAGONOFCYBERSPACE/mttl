#!/usr/bin/python3

import json
import os
import pymongo
import sys

# auto formatting will move mongo helpers up -
# this breaks that syspath must come before mongohelpers import
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from mongo_helpers import find_array_not_empty, find_ksat_id, find_array_empty

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links

filename = os.path.splitext(os.path.basename(__file__))[0]
log_data = {}
error = False


def report_orphan(orphan: dict):
    if orphan['ksat_id'][0] != 'T':
        _id = str(orphan['_id'])
        if _id not in log_data:
            log_data[_id] = {}
            log_data[_id].update({
                str(orphan['ksat_id']): 'does not have a parent'
            })


def check_task_parents(child: dict):
    if len(child['parent']) > 0:
        for parent in child['parent']:
            info = find_ksat_id(reqs, parent)
            doc = info[0]
            _id = str(child['_id'])
            if _id not in log_data:
                log_data[_id] = {}
                log_data[_id].update({str(doc['ksat_id']):
                                      'listed as parent to TASK '
                                      + child['ksat_id']
                                      })
            else:
                log_data[_id].update({str(doc['ksat_id']):
                                      'also listed as parent to TASK '
                                      + child['ksat_id']
                                      })


def check_ability_parents(child: dict):
    if len(child['parent']) > 0:
        for parent in child['parent']:
            info = find_ksat_id(reqs, parent)
            doc = info[0]
            parent_ksat_type = doc['ksat_id'][0]

            if parent_ksat_type != "T":
                _id = str(child['_id'])
                if _id not in log_data:
                    log_data[_id] = {}
                    log_data[_id].update({str(doc['ksat_id']):
                                          'listed as parent to ABILITY ' +
                                          child['ksat_id']
                                          })
                else:
                    log_data[_id].update({str(doc['ksat_id']):
                                          'also listed as parent to TASK ' +
                                          child['ksat_id']
                                          })


def check_skill_parents(child: dict):
    if len(child['parent']) > 0:
        for parent in child['parent']:
            info = find_ksat_id(reqs, parent)
            doc = info[0]
            parent_ksat_type = doc['ksat_id'][0]
            if parent_ksat_type != "T" or parent_ksat_type != "A":
                _id = str(child['_id'])
                if _id not in log_data:
                    log_data[_id] = {}
                    log_data[_id].update({str(doc['ksat_id']):
                                          'listed as parent to SKILL ' +
                                          child['ksat_id']
                                          })
                else:
                    log_data[_id].update({str(doc['ksat_id']):
                                          'also listed as parent to SKILL ' +
                                          child['ksat_id']
                                          })


def check_parent(child: dict):
    child_type = child['ksat_id'][0]
    if child_type == 'T':
        check_task_parents(child)
    elif child_type == 'A':
        check_ability_parents(child)


def main():

    for item in find_array_empty(reqs, 'parent'):
        report_orphan(item)

    for item in find_array_not_empty(reqs, 'parent'):
        check_parent(item)

    if len(log_data) > 0:
        with open(f'{filename}.json', 'w') as log_file:
            json.dump(log_data, log_file, sort_keys=False, indent=4)
        exit(1)
    # iterate all KSATs with parents
    # for item in find_array_not_empty(reqs, 'parent'):
    #      print(item['ksat_id'], "I'm not an orphan!")
    # for item in find_array_not_empty(reqs, 'parent'):
    #     for parent in item['parent']:
    #         find_id_and_append_array(
    #             reqs,
    #             parent,
    #             'children',
    #             item['ksat_id'],
    #             upsert=False
    #         )


if __name__ == "__main__":
    main()
