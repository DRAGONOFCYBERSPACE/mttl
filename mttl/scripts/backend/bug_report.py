#!/usr/bin/env python3
import os
import requests
import sys
import gitlab_email
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504]
    )
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")

title = sys.argv[1]
desc = sys.argv[2]
username = sys.argv[3]
email_blurb = gitlab_email.get_email_blurb(username)
project_id = 18738190

data = {
    'title': 'Bug Report: ' + title,
    'description': "@" + username + " submitted a bug report " + """


Description: """ + desc + """

""" + email_blurb + """
/label ~customer ~"office::CYT" ~"BUG" ~USER_SUBMISSIONS ~"CYT::MTTL"
/milestone %"CYT Backlog" """
}

res = gl.post(
    gitlab_api+'/'+str(project_id)+'/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token}
)

try:
    print(res.json()['web_url'])
except KeyError:
    print("none")
