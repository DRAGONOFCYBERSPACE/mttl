#!/usr/bin/env python3
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from urllib.parse import unquote
import requests
import sys
import os
# just comment out import json if not in use
# import json


gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))
api_token = os.getenv("GOOGLE_TOKEN")


def check_link(link):
    api_key = api_token
    # headers = {'Content-type': 'application/json'}
    request_url = "https://safebrowsing.googleapis.com/"\
        + "v4/threatMatches:find"
    threat_types = "MALWARE", "SOCIAL_ENGINEERING", "UNWANTED_SOFTWARE",\
        "POTENTIALLY_HARMFUL_APPLICATION", "THREAT_TYPE_UNSPECIFIED"
    platform_types = "ANY_PLATFORM"
    threat_entry_type = "THREAT_ENTRY_TYPE_UNSPECIFIED", "URL", "EXECUTABLE"
    threat_entry = link
    client_info = {'clientId': "90 COS", 'clientVersion': "0.0.1"}

    threat_info = {'threatTypes': [threat_types],
                   'platformTypes': [platform_types],
                   'threatEntryTypes': [threat_entry_type],
                   'threatEntries': [{'url': threat_entry}]
                   }
    params = {'key': api_key}
    request_info = {'client': client_info, 'threatInfo': threat_info}

    # print("client info" , clientInfo)
    # print("threat info", threatInfo)
    # print ("request info", request_info)
    # print ("request url: ", request_url)
    res = requests.post(request_url,
                        params=params,
                        json=request_info,
                        timeout=(3, 10)
                        )
    try:
        response = res.json()
        # content_type = res.headers.get('Content-Type', '')
        content_type = get_contenttype_header(link)
        return(response, link, content_type)
    except():
        return None


def parse_response(res):
    # a list  of  dictionaries
    the_danger = ''

    response = res[0]
    link = res[1]
    content_type = res[2]
    output = "{"
    if "matches" in response:
        danger = response.items()
        the_danger = (next(iter(danger))[1][0])
        threat = (the_danger['threatType'])
        output += '"threat": "' + threat + '",'
        # print(theDanger['platformType'])
        # print(theDanger['threat']['url'])
        # print(theDanger['cacheDuration'])
        # print(theDanger['threatEntryType'])
        # for item in theDanger:
        #     print(item)
    else:
        link_str = '"link": "' + link + '", '
        no_threat = '"threat": "none identified", '
        no_threat_message = link_str + no_threat
        output += no_threat_message
        # if key == "threat":
        #     urlList = value
        #     print(value['url'])
    output += '"content-type": "' + content_type + '"}'
    return output


def get_contenttype_header(link):
    try:
        page = requests.get(
            link, headers={'user-agent': '90COS/0.0.1'}, timeout=(3, 10))
        header = page.headers['Content-Type']
        return header
    except ConnectionError:
        print("**flag** Error Connecting")
        return


def main():
    if len(sys.argv) < 2:
        print("Link was not given. This is an error. Exiting")
        sys.exit()
    else:
        link = unquote(sys.argv[1])

    if (link.find('http://') != 0 and link.find('https://') != 0):
        link = "http://" + link

    if (link.find("duckduckgo") != -1):
        parsed_response = parse_response(({},
                                          get_contenttype_header(link),
                                          link
                                          ))
        print(parsed_response)
        return parsed_response

    response = check_link(link)
    parsed_response = parse_response(response)
    print(parsed_response)
    return parsed_response


# API KEY [REMOVED]
# TOS https://developers.google.com/safe-browsing/terms
# more info on the safebrowsing API is here:
# https://developers.google.com/safe-browsing/v4/lookup-api
# empty responses are no known threat
# links to test safe browsing
# https://testsafebrowsing.appspot.com/
main()
