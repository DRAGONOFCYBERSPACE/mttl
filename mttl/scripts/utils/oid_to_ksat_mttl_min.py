#! /usr/bin/python3
import os
import json


# This program just returns a list of dictionaries for OID to KSAT

def main():
    print("This expects to find MTTL.min.json at:")
    print("../../src/data/MTTL.min.json")
    cur_path = os.path.dirname(__file__)
    mttl_file = os.path.relpath('../../src/data/MTTL.min.json', cur_path)

    with open(mttl_file, 'r') as f:
        mttl_data = json.load(f)

    ksat_oid_list = []
    for line in mttl_data:
        new_dict = {"oid": '', "ksat_id": ''}
        new_dict['oid'] = line['_id']
        new_dict['ksat_id'] = line['ksat_id']
        ksat_oid_list.append(new_dict)

    return ksat_oid_list


if __name__ == '__main__':
    main()
