#! /usr/bin/env python3

import json
import sys
import oid_to_ksat_mttl_min

ksat_ids = oid_to_ksat_mttl_min.main()
mapped_urls = []
url_datas = []


def get_ksat_from_oid(oid) -> str:
    for line in ksat_ids:
        if line['oid'] == oid:
            return line['ksat_id']


def has_this_url_been_mapped(url) -> bool:
    for item in mapped_urls:
        if url == item:
            return True
    return False


def add_new_supplementary_entry(url, ksat_dict, name):
    newdict = {"url": '', "name": '', "desc": '', "KSATs": ''}
    ksat_list = []
    ksat_list.append(ksat_dict)
    newdict['url'] = url
    newdict['name'] = name
    newdict['KSATs'] = ksat_list
    url_datas.append(newdict)


def append_supplementary_entry(url, ksat_dict):
    for entry in url_datas:
        if entry['url'] == url:
            entry['KSATs'].append(ksat_dict)


if len(sys.argv) > 1:
    rellink_file = sys.argv[1]

else:
    print("Specify rellink file to extract data from")
    print("Nothing to do. Exiting.")
    sys.exit()

# Open work role file
rel_f = open(rellink_file, "r")
data = json.load(rel_f)


for line in data:
    # print(line.keys())
    # print("this is a line \n\n\n", line)
    name = line['subject']
    for ksat in line['KSATs']:
        newdict = {'url': '', 'url_data': ''}
        url_data_dict = {'ksat_id': '', 'proficiency': ''}
        # print(line['KSATs'][0])

        oid = ((ksat['object_id']['$oid']))
        ksat_id = get_ksat_from_oid(oid)
        proficiency = ksat['item_proficiency']
        url = ksat['url']
        url_data_dict['ksat_id'] = ksat_id
        url_data_dict['proficiency'] = proficiency
        newdict['url'] = url
        newdict['url_data'] = url_data_dict
        if not has_this_url_been_mapped(url):
            add_new_supplementary_entry(url, url_data_dict, name)
            # print("adding new ", newdict)
            mapped_urls.append(url)
        else:
            append_supplementary_entry(url, url_data_dict)
            # print("appending ", newdict)

new_filename = "supplementary_" + rellink_file
with open(new_filename, "w") as new_file:
    new_file.write(json.dumps(url_datas, indent=4, sort_keys=False))
