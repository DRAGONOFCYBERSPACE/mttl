#! /usr/bin/python3
import csv
import sys


mttl_file = sys.argv[1]
ksat = sys.argv[2]

with open(mttl_file, 'rt') as csvfile:
    reader = csv.reader(csvfile, quotechar='"')
    for row in reader:
        if (row[2] == ksat):
            print(row[1])
