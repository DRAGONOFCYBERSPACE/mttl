#! /usr/bin/env python3

import json
import sys
import oid_to_ksat_mttl_min


# gets a list of oids to ksats
ksat_ids = oid_to_ksat_mttl_min.main()


def get_ksat_from_oid(oid) -> str:
    for line in ksat_ids:
        if line['oid'] == oid:
            return line['ksat_id']


if len(sys.argv) > 1:
    rellink_file = sys.argv[1]

else:
    print("Specify rellink file to remove id from")
    print("Nothing to do. Exiting.")
    sys.exit()

# Open work role file
rel_f = open(rellink_file, "r")
data = json.load(rel_f)


for line in data:
    for ksat in line['KSATs']:
        if 'object_id' in ksat.keys():
            oid = ksat['object_id']['$oid']
            del(ksat['object_id'])
            ksat_id = get_ksat_from_oid(oid)
            ksat['ksat_id'] = ksat_id
        else:
            print("'object_id' not found")


new_filename = "oid_free_" + rellink_file
with open(new_filename, "w") as new_file:
    new_file.write(json.dumps(data, indent=4, sort_keys=False))
