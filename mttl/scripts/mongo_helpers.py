def find_ksat_id(collection, id: str):
    '''
    MongoDB Query function to find ksat_id for _id
    '''
    return collection.find({'_id': id})


def find_array_not_empty(collection, field: str):
    '''
    MongoDB Query function to find documents with
    field (array type) that is not empty
    '''
    return collection.find({
        f'{field}': {'$not': {'$size': 0}}
    }, sort=[('_id', 1)])


def find_array_empty(collection, field: str):
    '''
    MongoDB Query function to find documents with
    field (array type) that is not empty
    '''
    return collection.find({
        f'{field}': {'$size': 0}
    }, sort=[('_id', 1)])


def find_id_and_append_array(collection,
                             find_id: str,
                             update_field: str,
                             value: [str, dict],
                             upsert: bool = False):
    '''
    MongoDB Query function to find one document with id
    '''
    return collection.update_one(
        {'_id': find_id},
        {'$push': {update_field: value}},
        upsert=upsert
    )


def find_id_and_modify(collection,
                       find_id: str,
                       update_field: str,
                       value: [str, list],
                       upsert: bool = False):
    '''
    MongoDB Query function to find one document with id
    '''
    # print(f"mongo helpers inserting value {value}")
    return collection.update_one(
        {'_id': find_id},
        {'$set': {update_field: value}},
        upsert=upsert
    )


def find_id_and_insert(collection,
                       find_id: str,
                       update_field: str,
                       value: [str, list],
                       upsert: bool = False):
    '''
    MongoDB Query function to find one document with id
    '''
    current_values = collection.find({'_id': find_id})
    existing_training_links = []
    new_links = value
    # there's only one
    for doc in current_values:
        existing_training_links = doc['training_links']

    all_links = list(set(existing_training_links + new_links))
    # print(f"mongo helpers inserting value {value}")
    return collection.update_one(
        {'_id': find_id},
        {'$set': {update_field: all_links}},
        upsert=upsert
    )


def key_picker(id: str):
    return {
        'knowledge': 'K',
        'skills': 'S',
        'abilities': 'A',
        'tasks': 'T'
    }[id.lower()]


def get_new_ksat_key(collection: object, ksat_type: str):
    '''
    Find the highest ksat id of a specific ksat type, increment, and return
    '''
    ksat_tag = key_picker(ksat_type)
    ksat_id = collection.find_one({
        'ksat_id': {'$regex': f'[{ksat_tag}][0-9]{{4}}'}
    }, sort=[('ksat_id', -1)])['ksat_id']
    num = ksat_id[1:]
    return f'{ksat_tag}{(int(num) + 1):04d}'
