# Performance Labs:

## Git Command Line:
[Link to Lab documentation](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/Git/git_command_line/instructions.md)

**Description:**
1) From the command line in your Exam Virtual Machine, create a clone of the Exam repository provided to you by the exam administrators.
2) Checkout the branch that you created in your GitLab merge request.
    + Take a screen shot of your current working branch name and save to your branch.
3) Commit and push your code often during your exam to minimize the possiblity of lost work.
4) Once your exam is complete:
    + Make a final commit with the message of Final Submission
    + Push your code to the remote branch.
    + Create a text file of your git log and save to your branch.
5) Return to GitLab problem for final step

------------------

## GitLab Issue Creation:
[Link to Lab documentation]()

**Scenario:**

You've previously developed a website that uses 3 buttons to start, pause, and stop an animation.  You just got your first color blind user and they can't tell which buttons do what actions as they are all sized and shaped exactly the same.  You use a seperate key and legend to say which colors mean what with only color as the difference.

**Description:**

1) Create an issue in your exam repository on gitlab.com to answer this question.
In the issue description, create a User Story that describes the requirement and a potential high level solution. You do not need to implement this solution.
    + The issue must contain an acceptance criteria.
    + Assign yourself to the issue.
    + The issue title must not contain the terms "master", "exam", "test", "branch", or your name

2) After you create the issue, create a merge request from a new branch based off of master. THIS BRANCH MUST NOT BE YOUR EXAM BRANCH. The branch name must NOT contain the terms "master", "exam", "test", "branch", or your name

3) Ensure the merge request is marked as Ready