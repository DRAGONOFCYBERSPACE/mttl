import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '../api.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ProfileService } from '../profile.service';
import * as halfmoon from 'halfmoon';
import { faBug, faSync } from '@fortawesome/free-solid-svg-icons';

export interface BugData{
  bug_short: string;
  bug_desc: string;
}

@Component({
  selector: 'app-bug-report',
  templateUrl: './bug-report.component.html',
  styleUrls: ['./bug-report.component.scss'],
})

export class BugReportComponent{
  error = false;
  thanks = false;
  faBug = faBug;
  syncIcon = faSync;
  url = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: BugData,
    public dialogRef: MatDialogRef<BugReportComponent>,
    private apiService: ApiService,
    private profileService: ProfileService
  ) { }

  async sendData(): Promise<any> {
    this.error=false;
    this.thanks=false;
    if(this.data.bug_short){
      try{
        this.thanks=true;
        console.log('Sending Data: ' + this.data.bug_short + ' Description: ' + this.data.bug_desc);
        this.url = await this.apiService.requestBugReport(this.data.bug_short, this.data.bug_desc);
      }catch(error){
        this.error=true;
        console.error('Error sending bug report ', error);
      }
    }
    return 1;
  }

  toggleDarkMode(): void {
    halfmoon.toggleDarkMode();
  }

}
