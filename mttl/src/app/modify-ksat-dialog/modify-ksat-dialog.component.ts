import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '../api.service';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { faSync } from '@fortawesome/free-solid-svg-icons';

import * as halfmoon from 'halfmoon';

export interface DialogData {
  ksatId: string;
  info: string;
}

@Component({
  selector: 'app-modify-ksat-dialog',
  templateUrl: './modify-ksat-dialog.component.html',
  styleUrls: ['./modify-ksat-dialog.component.scss']
})
export class ModifyKsatDialogComponent{

  public url: string;
  public spam: string[];
  public spamFlag: boolean;

  syncIcon = faSync;
  error = false;
  thanks = false;

  constructor(@Inject(MAT_DIALOG_DATA) public diagData: DialogData,
              public dialogRef: MatDialogRef<ModifyKsatDialogComponent>,
              private cookieService: CookieService,
              private http: HttpClient,
              private apiService: ApiService ) {}

  requestKsatModification(ksatID, desc): string {
    const token = this.cookieService.get('ProfileforMTTL');
    // console.log('newLink/' + ksatID + '/' + type + '/' + url);
    this.http.get('modifyKSAT/' + ksatID + '/' + encodeURIComponent(desc)
    + '/' + token, { responseType: 'text' })
    .subscribe(data=>{
      console.log('URL after data pull: ', data);
      this.url = data;
      this.spamCheck(this.url);
    });
    return this.url;
  }


  sendData(): void {
    if (this.thanks === true){
      this.reset();
    }
    this.error = false;
    this.thanks = false;

    if (this.diagData.info){
      this.requestKsatModification(this.diagData.ksatId, this.diagData.info);
      this.thanks = true;
    } else {
      this.error = true;
    }

  }

  reset(): void{
    this.error = false;
    this.thanks = false;
    this.spam = [''];
    this.spamFlag = false;
    this.url = '';
    return;
  }

  spamCheck(val: string): boolean{
    console.log('in spamCheck with: ', val);
    if(val){
      this.spam = val.match(/spam/gi);
      if(this.spam && this.spam.length > 0){
        this.spamFlag=true;
        console.log('Spam response');
      }else{
        this.spamFlag=false;
      }
    }else{
      //if nothing in val send spam
      this.spamFlag=true;
      console.log('Spam response');
    }
   return this.spamFlag;
  }

  click(): void{
    if(!this.spamFlag){
      window.open(this.url, '_blank');
    }
  }
}
