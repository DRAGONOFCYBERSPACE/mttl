import { Component, OnInit } from '@angular/core';
import { ContactUsComponent } from '../contact-us/contact-us-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import * as halfmoon from 'halfmoon';

import FAQData from './faq.json';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FAQComponent implements OnInit {

  contactEmail = '90IOS.DOT.INBOX@us.af.mil';
  search = faSearch;
  faqData = FAQData; // Making the imported data available to page

  constructor( private dialog: MatDialog) { }

  contactUs(): void {
    const dialogRef = this.dialog.open(ContactUsComponent, {
      height: '400px',
      width: '390px',
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }


  ngOnInit(): void {
    halfmoon.onDOMContentLoaded();
  }

}
