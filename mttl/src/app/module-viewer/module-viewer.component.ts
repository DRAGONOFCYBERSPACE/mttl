import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import * as halfmoon from 'halfmoon';

import { faArrowAltCircleLeft, faArrowAltCircleRight, faMapSigns, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-roadmap',
  templateUrl: './module-viewer.component.html',
  styleUrls: ['./module-viewer.component.scss']
})
export class ModuleViewerComponent implements OnInit {

  // Dict of roadmaps will be stored here.
  roadmaps;

  // Fa Icons
  rightArrow = faArrowAltCircleRight;
  leftArrow = faArrowAltCircleLeft;
  signs = faMapSigns;
  linkIcon = faExternalLinkAlt;

  currentRoadmap = 'kernel-module-1';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.roadmaps = this.apiService.getmodularRoadmap();
    console.log(this.roadmaps);
    // Default on ready function for halfmoon UI
    halfmoon.onDOMContentLoaded();
  }

  setRoadmap(value): void {
    this.currentRoadmap = value;
  }

}
