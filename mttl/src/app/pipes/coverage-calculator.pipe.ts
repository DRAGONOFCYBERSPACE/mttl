import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coverageCalculator'
})
export class CoverageCalculatorPipe implements PipeTransform {

  transform(items: any[], field: string): number[] {

    let itemList;

    // eval_links, training_links

    switch (field) {
      case 't_and_e':
        itemList = items.filter((el) => {
          if (el.eval_links.length > 0 && el.training_links.length > 0){
            return el;
          }
        });
        break;
      case 't_o':
        itemList = items.filter((el) => {
          if (el.eval_links.length === 0 && el.training_links.length > 0){
            return el;
          }
        });
        break;
      case 'e_o':
        itemList = items.filter((el) => {
          if (el.eval_links.length > 0 && el.training_links.length === 0){
            return el;
          }
        });
        break;
      case 'none':
        itemList = items.filter((el) => {
          if (el.eval_links.length === 0 && el.training_links.length === 0){
            return el;
          }
        });
        break;
    }

    return itemList;
  }

}
