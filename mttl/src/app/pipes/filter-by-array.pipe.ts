// | filterByArray: 'primary_position':['RHR','LHR']
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name : 'filterByArray'
})

export class FilterByArrayPipe implements PipeTransform {
    transform(items: any[], field: string, values): any[] {
        // items = ksats being displayed
        // values - work roles
        if (!items) { return []; }
        if (values[0] === 'All'){ return items; }
        if (values[0] === '1'){ values[0] = 1; }
        if (values[0] === '0'){ values[0] = 0; }

        const itemList = items.filter((el) => el[field] != null );
        return items.filter( it => values.some( ksat => it[field].includes(ksat) )  );
    }
}
