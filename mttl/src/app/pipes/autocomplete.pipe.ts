import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'autocomplete'
})
export class AutocompletePipe implements PipeTransform {

  transform(values: any [], text: string): string [] {
    const output = [];
    // console.log("Current Text: ", text)
    values.forEach(val => {
      if (typeof val === 'string' ){
        if (val.toLowerCase().indexOf(text.toLowerCase()) !== -1){
          output.push(val);
        }
      }
      else {
        if (val.key.toLowerCase().indexOf(text.toLowerCase()) !== -1){
          output.push(val);
        }
      }
    });

    return output;
  }

}
