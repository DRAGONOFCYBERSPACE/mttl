import { Injectable } from '@angular/core';
//Need this to get oauth info
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

// this is for redirect URI - maybe use express
import { environment } from '../environments/environment';

// observables
import { Observable, of, from, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CookieService } from 'ngx-cookie-service';
import { ApiService } from './api.service';
import { Subject } from 'rxjs';

//refresh
import { ApplicationRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  accessToken;
  state: string;
  code: string;
  cookieURL: string;
  profileCookieValue: string;
  client_id = 'f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1';
  part_two: string;
  redirect_uri: string;
  profileName: string;
  profileInfo;
  usernameInfo;
 // oknessInfo;
  isLoggedIn = false;
  redirected = false;
  tokenFromCode;
  hasProfileCookie = false;
  hasCode = false;
  hasToken = false;
  hasProfile = false;
  gitLabusername: string;
  //okness;
  farewell;
  canIaskForToken = true;


  profileNameChange: Subject<string> = new Subject<string>();
  profileInfoChange: Subject<string> = new Subject<string>();
  profileUserNameChange: Subject<string> = new Subject<string>();
  isOKchange: Subject<string> = new Subject<string>();

  //for Profile cookie
  chars = [...'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'];
  constructor(
    private apiService: ApiService,
    private http: HttpClient,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private appRef: ApplicationRef) {
    this.redirect_uri = environment.REDIRECT_URI;
    this.profileNameChange.subscribe((value) => {
      this.profileName = value;
    });
    this.profileInfoChange.subscribe((value) => {
      this.profileInfo = value;
    });
    this.profileUserNameChange.subscribe((value) => {
      this.gitLabusername = value;
    });
    // this.isOKchange.subscribe((value) => {
    //   this.okness = value;
    // });

    this.profileUserNameChange.subscribe((value) => {
      this.gitLabusername = value;
    });

  }

  randomString = length => {
    const random13chars = () => Math.random().toString(16).substring(2, 15);
    const loops = Math.ceil(length / 13);
    return new Array(loops).fill(random13chars).reduce((str, func) => str + func(), '').substring(0, length);
  };


  updateApplication() {
    this.appRef.tick ();
  }

  setProfileCookie(): void {
       if (localStorage.getItem('refreshed')) {
    localStorage.removeItem('refreshed');
    console.log('unrefreshed');
  }
    this.redirected = true;
    if (this.profileCookieValue === '') {
      console.log('no profile cookie found - creating cookie');
      this.state = this.randomString(90);
      // alert('Login with Gitlab for increased functionality');
      this.cookieService.set('ProfileforMTTL', this.state, 3);
      this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
      this.hasProfileCookie = true;
    } else {
      console.log('profile cookie found - retrieving cookie');
      this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
      this.hasProfileCookie = true;
    }
  }


  async getTokenWithCode(code) {
    this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
    // const accessToken =  ''
    if (this.canIaskForToken){
      this.canIaskForToken = false;
      this.http.get('oauth-submit/' + encodeURIComponent(code) + '/' + encodeURIComponent( this.profileCookieValue) + '/',)
      .subscribe(response => {
        console.log('profile -> serverJS - Getting Access Token, response: ', response);
        this.accessToken = response;

        return this.accessToken;
      },
        err => {
          console.log('error in getAccessToken ', err);
          return err;
        });
        if (this.accessToken  !== '' && this.accessToken !== undefined ){
          this.hasCode = true;
          this.hasToken = true;
          // this.accessToken = accessToken;
          // this.tokenFromCode = accessToken;
        }
        this.canIaskForToken = true;
        return;
    }
   else {
     console.log('not allowed to ask for token');
   }

  }

  // async getTokenWithCode(code) {
  //   this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
  //   const accessToken = await this.apiService.getAccessToken(code, this.profileCookieValue);
  //   this.hasCode = true;
  //   this.hasToken = true;
  //   this.accessToken = accessToken;
  //   this.tokenFromCode = accessToken;
  // }

  getTokenWithCookie() {
    this.cookieURL = '/gittoken/' + this.profileCookieValue + '/';
    this.http.get(this.cookieURL).subscribe(data => {
      const example = Object.keys(data).map(key => data[key]);
      this.accessToken = example[3][0].access_token;
      this.redirected = false;
      return example[3][0].access_token;
    });
  }

  getProfileWithCookie() {
    this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
    if (this.profileCookieValue !== '' && (this.profileInfo === undefined || this.profileInfo === null)) {
      this.http.get('/profile/' + this.profileCookieValue).subscribe(data => {
        this.profileInfo = Object.keys(data).map(key => data[key]);
        this.profileName = this.profileInfo[3][0].name;
        const type = typeof this.profileName;
        if (typeof this.profileName === 'undefined' || this.profileName === null) {
          this.isLoggedIn = false;
          console.log('profile service - not logged in via profile', this.isLoggedIn);
        } else {
          this.isLoggedIn = true;
          this.hasProfile = true;
          console.log('profile service - logged in via profile', this.isLoggedIn);
          if (!localStorage.getItem('refreshed')) {
            localStorage.setItem('refreshed', 'no reload');
            this.updateApplication();
          setTimeout(() => location.reload(), 90);
       //   setTimeout(window.location.reload.bind(window.location), 1200);
          }
        }
        console.log('profile service - profile name is ', this.profileName);
        this.profileInfoChange.next(this.profileInfo);
        this.profileNameChange.next(this.profileName);
        return this.profileInfo;
      });
    } else {
      return this.profileInfo;
    }
  }


  getUserNameWithCookie() {
    this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
    if (this.profileCookieValue !== '' && (this.gitLabusername === undefined || this.gitLabusername === null)) {
      this.http.get('/username/' + this.profileCookieValue).subscribe(data => {
        this.usernameInfo = Object.keys(data).map(key => data[key]);
        this.gitLabusername = this.usernameInfo[3][0].nickname;
        this.profileUserNameChange.next(this.gitLabusername);
        this.updateApplication();
        return this.usernameInfo;
      });
    } else {
      return this.usernameInfo;
    }
  }

  getIsLoggedIn() {
    return this.isLoggedIn;
  }
  getHasCode() {
    return this.hasCode;
  }
  getHasProfileCookie() {
    return this.hasProfileCookie;
  }
  getHasProfile() {
    return this.hasProfile;
  }

  getProfileInfo() {
    if (this.profileInfo !== undefined) {
      return this.profileInfo;
    } else {
      this.getProfileWithCookie().subscribe(data => {
        this.profileInfo = data;
        this.profileInfoChange.next(this.profileInfo);
        return this.profileInfo;
      });
    }
  }


  async getProfileName() {
    if (this.profileInfo !== undefined) {
      return this.profileName;
    } else {
      this.getProfileWithCookie().subscribe(data => {
        this.profileName = data;
        this.profileNameChange.next(this.profileName);
        return this.profileName;
      });
    }
  }

  async getUserName() {
    if (this.gitLabusername !== undefined) {
      return this.gitLabusername;
    } else {
      this.getUserNameWithCookie().subscribe(data => {
        this.gitLabusername = data;
        this.profileUserNameChange.next(this.gitLabusername);
        return this.gitLabusername;
      });
    }
  }


  removeProfileWithCookie() {
    this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
    if (this.profileCookieValue !== '' ) {
      this.http.get('/goodBye/' + this.profileCookieValue).subscribe(data => {
        console.log('good bye received', data);
        // this.farewell = Object.keys(data).map(key => data[key]);
        console.log('good bye, ', this.farewell);
        const type = typeof this.farewell;
        if (typeof this.profileName === 'undefined' || this.profileName === null) {
          this.isLoggedIn = false;
          console.log('not deleting login - not logged in via profile', this.isLoggedIn);
        } else {
          this.cookieService.delete('ProfileforMTTL');
          this.usernameInfo = '';
          this.profileInfo = '';
          this.gitLabusername = '';
          this.profileName = '';
          this.profileUserNameChange.next(this.gitLabusername);
          this.profileInfoChange.next(this.profileInfo);
          this.profileNameChange.next(this.profileName);
          this.isLoggedIn = false;
          this.hasProfile = false;
          console.log('profile service - logged in via profile', this.isLoggedIn);
          if (!localStorage.getItem('logged out')) {
            localStorage.setItem('logged out', 'no reload');
           // setTimeout(() => location.reload());
            setTimeout(window.location.reload.bind(window.location), 1200);
          }
        }
        console.log('profile service - profile name is ', this.profileName);
        this.updateApplication();
        return this.farewell;
      });
    } else {
      return this.farewell;
    }
  }


  removeAndRevokeProfileWithCookie() {
    this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
    if (this.profileCookieValue !== '' ) {
      this.http.get('/farewell/' + this.profileCookieValue).subscribe(data => {
        console.log('farewell received', data);
        // this.farewell = Object.keys(data).map(key => data[key]);
        console.log('farewell, ', this.farewell);
        const type = typeof this.farewell;
        if (typeof this.profileName === 'undefined' || this.profileName === null) {
          this.isLoggedIn = false;
          console.log('not deleting login - not logged in via profile', this.isLoggedIn);
        } else {
          this.cookieService.delete('ProfileforMTTL');
          this.usernameInfo = '';
          this.profileInfo = '';
          this.gitLabusername = '';
          this.profileName = '';
          this.profileUserNameChange.next(this.gitLabusername);
          this.profileInfoChange.next(this.profileInfo);
          this.profileNameChange.next(this.profileName);
          this.isLoggedIn = false;
          this.hasProfile = false;
          console.log('profile service - logged in via profile', this.isLoggedIn);
          if (!localStorage.getItem('logged out')) {
            localStorage.setItem('logged out', 'no reload');
          //  setTimeout(() => location.reload());
            setTimeout(window.location.reload.bind(window.location), 1200);
          }
        }
        console.log('profile service - profile name is ', this.profileName);
        this.updateApplication();
        return this.farewell;
      });
    } else {
      return this.farewell;
    }
  }


}
