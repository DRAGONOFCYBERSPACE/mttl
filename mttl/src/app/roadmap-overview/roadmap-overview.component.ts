import { Component, DoCheck, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';

import cytoscape from 'cytoscape';
import dagre from 'cytoscape-dagre';
import klay from 'cytoscape-klay';

@Component({
    selector: 'app-roadmap-overview',
    templateUrl: './roadmap-overview.component.html',
    styleUrls: ['./roadmap-overview.component.scss']
})
export class RoadmapOverviewComponent implements OnInit,DoCheck {
  @Output() private hoveredWR = new EventEmitter<Record<string, unknown>>();
  @Output() private enterRoadmap = new EventEmitter<boolean>();
  cy: any;

  constructor(
    private apiService: ApiService,
  ){}

  ngOnInit(){
    const hoverEvent = this.hoveredWR;
    const clickEvent = this.enterRoadmap;
    const nodeInfo = this.apiService.getRoadmap();

    cytoscape.use(dagre);
    this.cy = cytoscape({
        container: document.getElementById('cy'),
        style: [{
          selector: 'node',
          style: {
            label: (ele) => ele.data().name,
            'text-wrap': 'wrap',
            'text-halign':'center',
            'text-valign':'center',
            backgroundColor: (node) => node.data().has_ttl ? '#fff' : '#D0D0D0',
            'border-width': (node) => node.data().has_ttl ? '5' : '8',
            'border-style': (node) => node.data().has_ttl ? 'solid' : 'double',
            'border-color': (node) => node.data().has_ttl ? '#00308F' : '#808080',
            shape: 'round-rectangle',
            width: '300',
            height: '150',
            'font-size': '60pt',
            'font-family': 'sans-serif'
          }
        },
        {
          selector: ':parent',
          css: {
            'text-valign': 'top',
            'text-halign': 'center',
            'background-color': '#eee',
            'border-width': '0',
            padding: '0px',
            events: 'no'
          }
        },
        {
          selector: ':selected',
          css: {
            'border-color': 'black',
            'border-width': 5
          }
        },
        {
          selector: 'edge',
          style: {
            width: 6,
            'curve-style': 'taxi',
            'taxi-direction': 'downward',
            'target-arrow-shape': 'triangle',
            'target-arrow-color': '#808080',
            'line-color': '#808080',
            events: 'no'
          }
        },
        {
          selector: 'core',
          css: {
            'active-bg-size': 0
          }
        }]
      });
    this.addRoadmapNodes(this.cy);
    this.addRoadmapEdges(this.cy);
    this.cy.selectionType('single');
    this.cy.boxSelectionEnabled( false );

    this.cy.layout(
      { name: 'dagre',
        rankSep: 100,
        nodeSep: 75,
        ranker: 'network-simplex'
      }
      ).run();
    this.cy.bind('click', 'node', (evt) => {
        console.log( 'clicked ' + evt.target.id() );
        return clickEvent.emit(true);
    });
    this.cy.bind('mouseover', 'node', (evt) => {
      const nodeId = evt.target.id();
      evt.target.select();
      // console.log( 'hovered ' + nodeId );

      const result = nodeInfo.find(
        ({ id }) => id === nodeId );
      return hoverEvent.emit(result);
    });
    this.cy.bind('mouseout', 'node', (evt) => {
      evt.target.unselect();
      // console.log( 'unhovered ' + evt.target.id() );
  });
  }

  addRoadmapNodes(cy) {
    const roadmapData = this.apiService.getRoadmap().sort((a, b) => (a.id > b.id) ? 1 : -1).reverse();
    const uniqueGroups = [...new Set(roadmapData.map(item => item.group))].sort();
    const subGroups = [];

    uniqueGroups.forEach(element => {
      if (element){
        cy.add({
          data: {
            id: element,
            name: ''
          },
          grabbable: false
        });
      }
    });

    roadmapData.forEach(element => {
      if (element.subgroup){
        if (! subGroups.includes(element.subgroup)){
          cy.add({
            data: {
              id: element.subgroup,
              name: '',
              parent: element.group
            },
            grabbable: false
          });
          subGroups.push(element.subgroup);
        }
        cy.add({
          data: {
            id: element.id,
            name: element.name,
            parent: element.subgroup,
            has_ttl: element.has_ttl
          },
          grabbable: false
        });
      } else {
        cy.add({
          data: {
            id: element.id,
            name: element.name,
            parent: element.group,
            has_ttl: element.has_ttl
          },
          grabbable: false
        });
      }
    });

  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }


  addRoadmapEdges(cy) {
    const roadmapData = this.apiService.getRoadmap().sort();

    roadmapData.forEach(parentNode => {
      parentNode.upgrades.forEach(childNode => {
        cy.add(
          {
            data: {
              source: parentNode.id,
              target: childNode
            }
          }
        );
      });
    });
  }
  ngDoCheck(){
    this.cy.fit(this.cy, 10);
    this.cy.center();
  }
}
