import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkroleRoadmapComponent } from './workrole-roadmap.component';

describe('TestComponent', () => {
  let component: WorkroleRoadmapComponent;
  let fixture: ComponentFixture<WorkroleRoadmapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkroleRoadmapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkroleRoadmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
