import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../api.service';
import { CookieService } from 'ngx-cookie-service';


interface Column{
    displayName: string;
    id: string;
    checked: boolean;
    subColumns: Column[];
}

@Component({
  selector: 'app-select-columns',
  templateUrl: './select-columns.component.html',
  styleUrls: ['./select-columns.component.scss']
})

export class SelectColumnsComponent implements OnInit{
  dialogConfig: MatDialogConfig;
  colData: Column[];
  defaultCols: Column[]=[
    {displayName: 'ID', id:'ksat_id', checked: true, subColumns: [] },
    {displayName: 'Description', id:'description', checked: true, subColumns: [] },
    {displayName: 'Topic', id:'topic', checked: true, subColumns: [] },
    {displayName: 'Owner', id: 'requirement_owner', checked: true, subColumns: [] },
    {displayName: 'Source', id: 'requirement_src', checked: true, subColumns: [] },
    {displayName: 'Work Role', id: 'wr_spec', checked: true, subColumns: [
      {displayName: 'Work Role: Proficiency', id: 'proficiency_data', checked: false, subColumns: []},
    ]},
    {displayName: 'Children', id: 'child_count', checked: true, subColumns: []},
    {displayName: 'Parents', id: 'parent_count', checked: true, subColumns: []},
    {displayName: 'Training', id: 'training_count', checked: true, subColumns: []},
    {displayName: 'Evaluation', id: 'eval_count', checked: true, subColumns: []},
    {displayName: 'OID', id: 'OID', checked: false, subColumns: []},
    {displayName: 'Comments', id: 'comments', checked: false, subColumns: []},
  ];

  constructor(public dialogRef: MatDialogRef<SelectColumnsComponent>,
  private cookieService: CookieService){}

  ngOnInit() {
    //console.dir(`Dialog config: ${this.dialogConfig}`);
    // console.log('printing on init: ');
    if (this.cookieService.check('mttlcolprefs')
        && JSON.parse(this.cookieService.get('mttlcolprefs')).length === this.defaultCols.length) {
      this.colData = JSON.parse(this.cookieService.get('mttlcolprefs'));
    } else {
      this.colData = this.defaultCols;
    }
  }

  save() {
    this.cookieService.set('mttlcolprefs', JSON.stringify(this.colData));
    console.log(this.colData);
    this.dialogRef.close(this.colData);
  }

  close() {

  }

  toggleData(item){
    this.colData.find(elem => elem === item).checked = !this.colData.find(elem => elem === item);
  }

  selectAll() {
    this.colData.map(elem => elem.checked = true);
    this.colData.map(elem => elem.checked = true);
  }

  deselectAll() {
    this.colData.map(elem => elem.checked = false);
    this.colData.map(elem => elem.checked = false);
  }
}
