import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// Load in minified MTTL json
import MTTLData from '../data/MTTL.min.json';
import roadmapData from '../data/roadmap.json';
import IDFData from '../data/IDF_dataset.min.json';
import modular_roadmapData from './module-viewer/module-viewer.min.json';
import autocompleteData from '../data/autocomplete.min.json';
import extraData from '../data/extra_datasets.min.json';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  accessToken;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
    ) {}

  getAllKSATs(): any {
    // Basic function to return all ksats located in minified json
    MTTLData.forEach((elem) => {
      const wr = 'wr_spec';
      const cc = 'child_count';
      const ec = 'eval_count';
      const tc = 'training_count';
      const pc = 'parent_count';

      // Changes work-roles to wr_spec
      // This is legacy from when MTTL.min.json's field was workroles/specializations
      // TODO: Refactor all references to 'wr_spec' to work-roles
      elem[wr] = elem['work-roles'];

      // Get Child Count
      elem.children ? elem[cc] = (elem.children).split(', ').length : elem[cc] = 0;

      // Get Eval Count
      elem.eval_links ? elem[ec] = (elem.eval_links).split(', ').length : elem[ec] = 0;

      // Get Training Count
      elem.training_links ? elem[tc] = (elem.training_links).split(', ').length : elem[tc] = 0;

      // Get Parent Count
      elem[pc] = elem.parent.length;
    });
    return MTTLData;
  }

  getAllIDFKSATs(): any {
    // Basically like getAllKSATs for IDF only
    IDFData.forEach((elem) => {
      const wr = 'wr_spec';
      const cc = 'child_count';
      const ec = 'eval_count';
      const tc = 'training_count';
      const pc = 'parent_count';
      elem[wr] = elem['work-roles/specializations'];
      elem[wr] ? elem[wr] = elem[wr].replace(/\s/g, '').split(',') : elem[wr] = [];
      elem.children ? elem[cc] = (elem.children).split(', ').length : elem[cc] = 0;
      elem.eval_links ? elem[ec] = (elem.eval_links).split(', ').length : elem[ec] = 0;
      elem.training_links ? elem[tc] = (elem.training_links).split(', ').length : elem[tc] = 0;
      elem[pc] = elem.parent.length;
    });
    return IDFData;
  }

  getWrMetrics(): any {
    //extraData cannot be parsed using the same methods as above.
    //extraData is separated into Key:Value pairs instead of an array.
    const wr = 'wr_spec';
    extraData[wr] = extraData.wrspec;
    return extraData;
  }

  getNoWrMetrics(): any {
    const wr = 'nowrspec';
    extraData[wr] = extraData.nowrspec;
    return extraData;
  }

  getRoadmap(): any {
    return roadmapData;
  }

  getmodularRoadmap(): any {
    return modular_roadmapData;
  }

  getAutocomplete(): any {
    const search = 'search';
    const desc = 'description';
    const parent = 'parent';
    const wr = 'wr_spec';

    autocompleteData[search] = [];
    autocompleteData[desc] = [];
    autocompleteData[parent] = autocompleteData.ksat_id;
    // changed autocompleteData.work_role to autocomplete.requirement_owner
    // json data pulls requirement_owner which lines up with WR catagories.
    autocompleteData[wr] = autocompleteData.work_role;
    return autocompleteData;
  }

  async getUrlStatus(url): Promise<any> {
    const res = await this.http.get('checkUrl/' + url);
    return res;
  }

  async getUrlSafety(url): Promise<any> {
    if (url.includes('confluence.90cos.cdl.af.mil')){
      console.log('skipping confluence link!');
      return true;
    }
    const result = await this.http.get('checkUrlSafety/' + url);
    console.log('api checking safety for ', url);
    return result;
  }

  async requestBugReport(title, desc): Promise<any>{
    const token = this.cookieService.get('ProfileforMTTL');
    try {
      console.log('bugReport/'+ encodeURIComponent(title) + '/' + encodeURIComponent(desc));
      const res = await this.http.get('bugReport/'+ encodeURIComponent(title) + '/' + encodeURIComponent(desc)
      + '/' + token, { responseType: 'text' }).toPromise();
      return res;
    } catch (error) {
      console.log('Error was thrown while submitting bug report: ', error);
    }
    return;
  }

  /*
  async requestKsatModification(ksatID, desc): Promise<any> {
    const token = this.cookieService.get('ProfileforMTTL');
    // console.log('newLink/' + ksatID + '/' + type + '/' + url);
    const res = await this.http.get('modifyKSAT/' + ksatID + '/' + encodeURIComponent(desc)
    + '/' + token, { responseType: 'text' }).toPromise();
    return 1;
  }


  async requestKsatLink(url, type, ksatID, safety): Promise<any> {
    const token = this.cookieService.get('ProfileforMTTL');
    console.log('newLink/' + ksatID + '/' + type + '/' + token +
    '/' +  encodeURIComponent(safety) + '/' +  encodeURIComponent(url));
    const res = await this.http.get('newLink/' + ksatID + '/' + type + '/' + token +
                           '/' +  encodeURIComponent(safety) + '/' +  encodeURIComponent(url), { responseType: 'text' }).toPromise();
    // res is a string that has been sent from server
    console.log("Response from server.js: ", res);
    return res;
  }
  requestNewKsat(name, topic, workrole, reqOwner,
    reqSrc, parents, children, trainingLink, trainingRef, evals, desc): any {
    const token = this.cookieService.get('ProfileforMTTL');
    console.log('api service identified this token for new ksat -> ', token);

    if (!name) {
      name = 'None Provided';
    }
    if (!reqOwner) {
      reqOwner = 'None Provided';
    }
    if (!reqSrc) {
      reqSrc = 'None Provided';
    }
    if (!parents) {
      parents = 'None Provided';
    }
    if (!children) {
      children = 'None Provided';
    }
    if (!evals) {
      evals = 'None Provided';
    }
    if (!trainingLink) {
      trainingLink = 'None Provided';
    }
    if (!trainingRef) {
      trainingRef = 'None Provided';
    }

    console.log('api service sending newksat: ', encodeURIComponent(name)
    + '/' + encodeURIComponent(topic) + '/' + encodeURIComponent(workrole) + '/' + encodeURIComponent(reqOwner)
    + '/' + encodeURIComponent(reqSrc) + '/' + encodeURIComponent(parents) + '/' + encodeURIComponent(children)
    + '/' + encodeURIComponent(trainingLink) + '/' + encodeURIComponent(evals) + '/' + encodeURIComponent(desc)
    + '/' + token );

    const url: string = this.http.get('newKsat/' + encodeURIComponent(name)
      + '/' + encodeURIComponent(topic) + '/' + encodeURIComponent(workrole) + '/' + encodeURIComponent(reqOwner)
      + '/' + encodeURIComponent(reqSrc) + '/' + encodeURIComponent(parents) + '/' + encodeURIComponent(children)
      + '/' + encodeURIComponent(trainingLink) + '/' + encodeURIComponent(trainingRef) + '/'
      + encodeURIComponent(evals) + '/' + encodeURIComponent(desc) + '/'
      + token, { responseType: 'text' })
      .subscribe( data => {
        console.log('URL after data pull: ', data);
      });
    //return;
  }*/


  async requestContact(desc, topic, notes): Promise<any> {
    const token = this.cookieService.get('ProfileforMTTL');
    if (!topic) {
      topic = 'None Provided';
    }
    if (!notes) {
      notes = 'None Provided';
    }
    const res = await this.http.get('newContact/' + encodeURIComponent(desc) + '/' + encodeURIComponent(topic)
      + '/' + token + '/' + encodeURIComponent(notes), { responseType: 'text' }).toPromise();
    return res;
  }

  async sendJSON(theJSON): Promise<any> {
    const token = this.cookieService.get('ProfileforMTTL');
    console.log('newJSON/' + token + '/' + encodeURIComponent(theJSON));
    const res = await this.http.get('newJSON/' + token + '/' + encodeURIComponent(theJSON)).toPromise();
    return 1;
  }

  //#                    _   _
  //#   ___   __ _ _   _| |_| |__
  //#  / _ \ / _` | | | | __| '_ \
  //# | (_) | (_| | |_| | |_| | | |
  //#  \___/ \__,_|\__,_|\__|_| |_|
  //#

  getAccessToken(code, state) {
    console.log('calling getAccessToken API');
    this.http.get('oauth-submit/' + encodeURIComponent(code) + '/' + encodeURIComponent(state) + '/',)
      .subscribe(response => {
        console.log('serverJS - Getting Access Token, response: ', response);
        this.accessToken = response;
        return this.accessToken;
      },
        err => {
          console.log('error in getAccessToken ', err);
          return err;
        });
    // return this.accessToken;
  }


  async sendCode(code): Promise<any> {
    console.log('sending code from API to get access tokens');
    console.log('oauth-submit/' + code);
    const res = await this.http.get('oauth-submit/' + encodeURIComponent(code) + '/', { responseType: 'text' })
      .subscribe(data => {
        console.log('api console', JSON.parse(data));
        this.accessToken = JSON.parse(data);
        const access = this.accessToken;
        // console.log('access_token', JSON.parse(access).access_token);
        // console.log('api service access token receipt:', JSON.parse(this.accessToken).access_token);
        return JSON.parse(this.accessToken).access_token;
      });
    return res;
  }
}
