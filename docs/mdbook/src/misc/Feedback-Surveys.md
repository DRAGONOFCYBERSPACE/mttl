#### Table of Content
- [New Employee Survey: Supervisor](#new-employee-survey-supervisor)
- [New Employee Survey: Employee](#new-employee-survey-employee)
- [Post Training Event Survey: Employee](#post-training-event-survey-employee)
- [Post Training Event Survey: Supervisor](#post-training-event-survey-supervisor)
- [Training Coverage Survey](#training-coverage-survey)

# New Employee Survey: Supervisor
How well is the new member performing?

# New Employee Survey: Employee
Why: We want to measure how well IQT and New Employee training prepares new employees to perform their job.

Alternative 1: Interview
What: Interview with New employees
When: 60 days after arrival? (need to determine timeline)
Who: 
- Execution: SEE? SELO? Tech Lead? Supervisor? Training?
- Analysis: SEE? SELO? Tech Lead? Supervisor? Training?
Where: In-person


Alternative 2:
What: Online survey with New employees
When: 60 days after arrival? (need to determine timeline)
Who: Automated System with 
Where: In-person

# Post Training Event Survey: Employee
Did you feel this survey made you better at your job?

# Post Training Event Survey: Supervisor
Flesh out but did this training make your employee better at their job?

# Training Coverage Survey
**Purpose:**
Establish a feedback loop from courses to trace back to individual KSAT items in order to ensure courses meet their advertised curriculum and justify value of courses.

**Uses:**
The course survey will be used to assist surveyors in evaluating whether the course they are attending meets the 90th's requirements in the proficiencies for KSAT items.
This feedback will be gathered and used to evaluate the value of courses and mapping of KSAT items with their associated proficiency codes. The surveys will be tied into the Jira workflow for requesting course and will be compulsory to complete the workflow.


What types of courses will these surveys be used for:
- Internal courses
- External courses
- Commercial courses


When do the surveys get administered?
**Starting for courses in August 2020**
- For internal courses, the instructor will provide time at the end of each topic to take the survey.
- For external courses, the designated class leader will give out a link to the surveys.
- For commercial courses, if a **class leader role is assigned**, they will be responsible for giving out the surveys. If the **class leader role is not assigned**, the individual trainee will be given links to the surveys and encouraged to fill out when time allows.

To encourage honest feedback and make the surveys more manageable, the course surveys are broken down by topic. If the topic is not present for the course, it will be broken down by the the subject level.


How often is the feedback cycle?
- At the end of every course.


What do we do with this feedback?
- Determine if remapping is needed.
- Evaluate the value that this course gives the trainee and 90th.
- Identify gaps in training and mapping(too high or too low).

What do we identify in the feedback?
- If specific KSAT items are taught.
- The level/proficiency at which the KSAT item is taught.

Additional steps that need to be taken:
* [ ] Get feedback/input from Caroline and Joe Aten.
* [ ] Seed with some feedback for testing metric identification.