## Routing Module

`./mttl/src/app/app-routing.module.ts`

### Routing Logic

This module will route everything that is not defined in server.js. Here are the current valid routes:

```javascript
const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'mttl', component: MttlComponent},
  {path: 'mttl/:workrole', component: MttlComponent},
  {path: 'misc-tables', component: MiscTablesComponent },
  {path: 'metrics', component: MetricsComponent},
  {path: 'wr-metrics', component: WrMetricsComponent},
  {path: 'all_metrics', component: AllMetricsComponent},
  {path: 'roadmap', component: RoadmapComponent },
  {path: 'modular-roadmap', component: ModularRoadmapComponent },
  {path: 'module-maker', component: ModuleMakerComponent },
  {path: 'contact_us', component: ContactUsComponent},
  {path: 'faq', component: FAQComponent},
  {path: 'bug-report', component: BugReportComponent},
  {path: 'disclaimer', component: DisclaimerComponent},
  {path: '**', component: PageNotFoundComponent}
  // Variables can be passed to components through the url structure
  // The variable name is what you will end up importing in the component
  // {path: 'URL/:IMPORTVARIABLE', component: ComponentName}
];
```

Any page that is not /, mttl, metrics, or all_metrics will be routed to the 404 page.

### Adding a New Route

If you want to add a new page to the MTTL, first create an [Angular Component](https://angular.io/cli/generate), then add it anywhere above the 404 page.

For example, if I created a knock knock joke page:
```javascript
const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'mttl', component: MttlComponent},
  {path: 'mttl/:workrole', component: MttlComponent},
  {path: 'misc-tables', component: MiscTablesComponent },
  {path: 'metrics', component: MetricsComponent},
  {path: 'wr-metrics', component: WrMetricsComponent},
  {path: 'all_metrics', component: AllMetricsComponent},
  {path: 'roadmap', component: RoadmapComponent },
  {path: 'modular-roadmap', component: ModularRoadmapComponent },
  {path: 'module-maker', component: ModuleMakerComponent },
  {path: 'contact_us', component: ContactUsComponent},
  {path: 'faq', component: FAQComponent},
  {path: 'bug-report', component: BugReportComponent},
  {path: 'disclaimer', component: DisclaimerComponent},
  {path: 'KnockKnockJokes', component: KnockKnockComponent }, // <---- The new component goes here
  {path: '**', component: PageNotFoundComponent}
  // Variables can be passed to components through the url structure
  // The variable name is what you will end up importing in the component
  // {path: 'URL/:IMPORTVARIABLE', component: ComponentName}
];
```
