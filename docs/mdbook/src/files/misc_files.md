## A brief description of other potentially important files 



| file                    | purpose                                                                                               | related commands  (if applicable)                      | recommended resources                                                                  |
|-------------------------|-------------------------------------------------------------------------------------------------------|--------------------------------------------------------|----------------------------------------------------------------------------------------|
| *.md                    | These are markdown files used for documentation.                                                      | mdbook; mdbook-mermaid                                | https://www.markdownguide.org/cheat-sheet/                                             |
| book.toml               | Top level file for generating markdown books                                                          | mdbook build; mdbook-mermaid install;  mdbook serve .  | It is encouraged to read this file. However, it is unlikely that it need to be edited. |
| mermaid.css             | This helps compile mermaid charts locally.                                                            | mdbook-mermaid install                                 | https://github.com/badboy/mdbook-mermaid ; to run mermaid locally, you will need to install `cargo` then `cargo install mdbook-mermaid`                   |
| ***build_frontend.sh*** | This script is to generate the MTTL functionality, ExpressJS, MD Books, and the Angular app locally.  |                                                        |                                                                                        |
| .gitlab/issue_templates | The directory where default issue templates for gitlab are housed                                     |                                                        | https://docs.gitlab.com/ee/user/project/description_templates.html                     |
|                         |                                                                                                       |                                                        |                                                                                        |
| carousel.css            | This file contains an edited version of bootstrap's `carousel` element. No other bootstrap included. | mttl/assets/css/carousel.css                        |                                                                                         |
| Dockerfile | This defines the environment for production, this build relies upon artifacts created by the pipeline | | https://docs.docker.com/engine/reference/builder/ |
| Dockerfile-dev | This defines the environment for the development environment | | https://docs.docker.com/engine/reference/builder/ |
| public/index.html | This file handles redirects from the legacy gitlab pages location | | |
| before_script.sh | This script loads the database files into a mogno database | ./mttl/scripts/before_script.sh | |
| after_script.sh | This script exports the mongo database into database files | ./mttl/scripts/after_script.sh | |
| server.js | This file runs the backend that allows Angular to call database and python functionality | node start server.js | |
