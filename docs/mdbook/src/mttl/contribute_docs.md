## Contributing to this Documentation

If there is a problem in this documentation, we are happy to take input and contributions to this MDBook from the public.

### Submitting an Issue Ticket

If there is a problem in this documentation that you would like to bring to our attention, please create a [new ticket](https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Development) and describe the documentation issue there.

### Editing the MDBook

Of course, we are always happy to accept contributions, and anyone with a 90COS GitLab account is able to contribute. All of this documentation is found in the docs directory in the MTTL repo. All pages are generated from [markdown](https://www.markdownguide.org/basic-syntax/), which makes these pages easy to modify. These pages are located in `./docs/mdbook` directory of the MTTL Repo.

If you add a page to this documentation, make sure that it is linked in the SUMMARY.md page. SUMMARY.md is very particular in what it will and won't accept. A summary mdbook has fewer formatting options than other MDBooks and will disregard HTML. For further details see [here](https://rust-lang.github.io/mdBook/format/summary.html).
