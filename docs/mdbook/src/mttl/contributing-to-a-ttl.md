# Contributing to a TTL

A TTL or Training Task List is use to define work-role specific requirements. Anyone  with a gitlab account can fork and contribute to work-role requirements. Any changes will be reviewed, and accepted or rejected by designated work-role owners before contributions are reflected in production.

The MTTL contains a 'work-roles' folder with many `.json` files describing requirements for each work role at the 90COS. An example of a single requirement in this file is shown and described below. Edit or add requirements to these JSON files following the [Change Management Workflow](https://gitlab.com/90cos/mttl/-/wikis/Home#change-management-workflow) documented on the MTTL Home page.

| Field | Required? | Description |
|-------|-----------|---------|
| ksat_id | required | The KSAT identifier found in the 'requirements/*.json' files |
| work-role | required | The name of the work-role that this requirement belong to |
| proficiency | required but can be '' | The level of ability for the requirement in question |

```json
{
	"ksat_id": "S0028",
    "work-role": "CCD",
    "proficiency": "B"
}
```