# <div style="text-align: center;">The MTTL Bot</div> 

<div style="text-align: center;">
<img  src="../uploads/mttl-bot-profile.png" alt="Work Role Roadmap" width="360"/>  
</div>
  
  --- 

## What is the MTTL bot?
The MTTL bot is available to intercede in automated pipelines and requests. 
___  


## What does the MTTL Bot?
The MTTL Bot will intercede on behalf of site visitors and submit GitLab issues at their behest.   
For example, the MTTL Bot is used for:
 - contact us issues
 - submitting training and eval links
  
***Additionally***, the MTTL bot may intercede for CYT developers. For example,
-  the MTTL Bot will attempt to lint and submit corrected Python at the dev's behest i.e. if a commit message is made that includes `--fix` 
-  The MTTL bot will also generate a review app with oauth functionality if a commit message includes the text `--oauth-review `


---  

## How do I use the MTTL Bot?
CYT developers have access to the MTTL Bot token.   

---  

## Is the MTTL Bot ***dangerous***?  
The MTTL Bot has very limited permissions. **Though** the MTTL Bot token must be guarded carefully lest it be used to generate chaos.    



