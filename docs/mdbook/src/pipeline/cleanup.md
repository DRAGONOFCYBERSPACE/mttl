## Cleanup Stage

### What is it?

This CI/CD stage, which only triggers on pipelines that include a review app (but not production) is the job that ensures that our review apps shut down after a predefined amount of time (generally 8 hours). This job can also be triggered manually and whenever the branch on which this was run is merged into master

### What does it do

Cleanup will use the autodeploy script to delete the review pod in the environment in which the pipeline deployed it (either oauth-review or a standard review app), which was created in the deploy stage.

**Note**: If you are logged into the kubernetes cluster, do not attempt to delete review namespaces. There is a known bug where manually deleting a namespace makes it dead to our CI pipeline forever.
