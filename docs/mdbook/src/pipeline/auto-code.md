## auto-code

**What this job does**: This is an ***optional job*** that attempts to make Python code pep8 compliant. 

**How to invoke this job**: The auto-fix functionality is activated by adding `--fix` to the commit message.

**What makes this job fail**: This job will not fail. However, it may not be able to fix all Python linting issues. 

**Scripts Used**: This job basically executes `autopep8 --ignore=E402 --in-place --recursive .` and recommits the changes as the [MTTL Bot](../dev/MTTL-BOT.md). 