## Pre-Build Stage

This stage handles jobs that need to be done before the data-build stage

### Create Autocomplete
**What this job does**: This CI job goes through the database, getting topics, WRs, owners, and sources to populate the autocomplete boxes on the MTTL page

**What makes this job fail**: This job fails if either of the scripts fail. This typically doesn't happen, so check the error message for more info.

**Scripts Used**: The script that generates autocomplete.min.json are located located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/pre_build).


### Fetch Rel-Links & Children

**What this job does**: This CI job goes through and:
1. Finds and adds the children for each KSAT, adding it to the children key in the JSON file.
1. Finds and adds all training and eval links for each KSAT

**What makes this job fail**: This job fails if either of the scripts fail. This typically doesn't happen, so check the error message for more info.

**Scripts Used**: The scripts that perform these changes are located located [here](https://gitlab.com/90cos/mttl/-/tree/master/mttl/scripts/pre_build).