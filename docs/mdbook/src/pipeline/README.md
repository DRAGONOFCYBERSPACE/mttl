## CI pipeline Documentation

This is the documentation for this project's use of GitLab's CI/CD Pipeline feature. General documentation for GitLab CI/CD can be found [here](https://docs.gitlab.com/ee/ci/).

This section contains documentation for our CI pipeline for the MTTL. 

The MTTL pipeline is divided into these stages:
1. [*auto-code](auto-code.md)
2. [*test](security-scans.md)
3. [quality](quality.md)
4. [unit-tests](tests.md)
5. [pre-build](prebuild.md)
6. [generate-data](data.md)
7. [project-build](angularbuild.md)
8. [containerize](containerize.md)
9.  [*container_scanning](security-scans.md)
10. [review](reviewprod.md)
11. [*performance](security-scans.md)
12. [production](reviewprod.md)
13. [cleanup](cleanup.md)

--- 
***'&#42;' indicates optional stages***

<!-- 16. 

4.  [review/production](reviewprod.md) -->