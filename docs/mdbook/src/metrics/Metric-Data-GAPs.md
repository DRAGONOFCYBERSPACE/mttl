## GAPs in Metric Data of the MTTLv2 repo

* No work role coverage data per KSA&T for Evaluation Questions
* No information showing KSA&T without a proficiency code
* The following metrics may or may not belong in the MTTL coverage report but are nonetheless gathered in the MTTL artifact and should be captured in the appropriate location to ensure they are collected/reported
  * Lessons/Questions with invalid KSA Assignments
  * Lessons/Questions with Invalid Work Role Assignments
  * Lessons/Questions without KSA Assignments
