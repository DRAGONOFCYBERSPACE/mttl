## **Determined naming convention**

We would like to have a determined naming convention for Modules, Sequences, and Units.
A standard naming convention for unique IDs to apply to Modules, Sequences, and Units so that reference modules can be created programmatically. 
This has been determined per ticket [#623](https://gitlab.com/90cos/mttl/-/issues/623).

### Naming Convention: 

#### Modules: 
Modules will have the following template for programmatically created UIDs:
*mod_[0-9]{4}*
The module will also have a human readable title which will be an alternate value within the data structure. 

#### Sequences: 
Sequences will have the following template for programmatically created UIDs:
*seq_[0-9]{4}*
The data structure for sequences will also allow for a human readable title.

#### Units:
Units will have the following template for creating UIDs:
*unit_[0-9]{4}*
The data structure for units will allow for a human readable title but will not be necessary.
