#!/bin/bash

RED='\033[0;31m'
ORANGE='\033[0;33m'
PURPLE='\033[0;35m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
tput setaf 2
tput setab 0

echo "          ____        _ _     _   __  __ _____ _____ _      "
echo " _ __ ___| __ ) _   _(_) | __| | |  \/  |_   _|_   _| |     "
echo "| '__/ _ \  _ \| | | | | |/ _  | | |\/| | | |   | | | |     "
echo "| | |  __/ |_) | |_| | | | (_| | | |  | | | |   | | | |___  "
echo "|_|  \___|____/ \__,_|_|_|\__,_| |_|  |_| |_|   |_| |_____| "
echo "                                                            "
printf "\n${RED}No data generated with rebuild\n"
tput sgr0

# no data is built - this is just node and angular
# Preventing port in use error
printf "\n${ORANGE}Looking for and shutting down all Node servers\n"
printf "\n${RED}WARNING: This script is meant to be run in a Docker container\n"
printf "\n${PURPLE}All Node servers will be shut down!\n"
for i in $(ps -aux | grep "node server.js" | awk '{ print $2 }')
do
    kill $i
done
# Run data scripts
tput dim 


tput smso
printf "${RED} Note - to use oauth - you will need to set the PART_TWO environment variables:\n"

printf "${ORANGE}  e.g. ${PURPLE} export PART_TWO='your part two'\n"
[[ ! -z "$PART_TWO" ]] && printf "${GREEN}PART_TWO provided\n" || printf "${RED} please - export PART_TWO \n"

# export REDIRECT_URI='http%3A%2F%2Flocalhost%3A5000%2Fredirect'
export REDIRECT_URI='http%3A%2F%2Flocalhost%3A5000%2Fmttl'
export PGCONN="postgres://mttl_user@postgres:5432/mttl"
export GLCONN="postgres://mttl_user@postgres:5432/gitlab"
export DATABASE_URL="postgres://mttl_user@postgres:5432/mttl"

echo "Building Angular"
# Build node
cd mttl
ng build

cd ..

echo "Starting expressJS server"
npm start &
sleep 5

# export redirect_uri='http%3A%2F%2Flocalhost%3A4200%2Fredirect'
[[ ! -z "$PART_TWO" ]] && printf "${GREEN}PART_TWO provided\n" || printf "${RED} Oauth will not work\n${PURPLE}Please - ${RED}export PART_TWO \n"
[[ ! -z "$ROBOTOKEN" ]] && printf "${GREEN}ROBOTOKEN provided\n" || printf "${RED} Oauth will not work\n${PURPLE}Please - ${RED}export ROBOTOKEN \n"
[[ ! -z "$GOOGLE_TOKEN" ]] && printf "${GREEN}GOOGLE_TOKEN provided\n" || printf "${RED} Link safety checks will not work\n${PURPLE}Please - ${RED}export GOOGLE_TOKEN \n"

printf "${GREEN}Frontend built!\n"
printf "${ORANGE}Visit http://localhost:5000\n"
tput sgr0